function qq = eval_dis_gendist(s,t,map_gen_id,map_gen_dist,penalflag)

%Disease trajectory clustering software
%Copyright (C) 2020 Alexia Giannoula and Laura I. Furlong, IBI group.
%This file is part of the Disease trajectory and clustering software

%The Disease trajectory and clustering software is free software: you can redistribute it and/or modify it under 
%the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 
%of the License, or (at your option) any later version (https://www.gnu.org/licenses/gpl-3.0.html).
 
%Disease trajectory and clustering software is distributed in the hope that it will be useful,but WITHOUT 
%ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
%See the GNU General Public License for more details.
 
%You should have received a copy of the GNU General Public License
%along with this program.  If not, see <http://www.gnu.org/licenses/>.
 
%How to cite Disease trajectory and clustering software:
%Giannoula, A., Centeno, E., Mayer M.-A., Sanz, F., & Furlong, L. I. A system-level analysis of patient disease 
%trajectories based on clinical, phenotypic and molecular similarities. Submitted to Bioinformatics.

jaccard = @(A, B) numel(intersect(A, B))./numel(union(A, B));

cmp_r_t = strcmp(s,t);
if (cmp_r_t == 0)
    A = map_gen_id(s); 
    B = map_gen_id(t); 
    penal1 = 1; 
    penal2 = 1;
    if (penalflag == 1)
        w1 = map_gen_dist(s); 
        w2 = map_gen_dist(t);
        if (w1>0 & w1 <1)
            penal1 = w1;        
        end
        if (w2>0 & w2 <1)
            penal2 = w2;        
        end
    end
    qq = penal1*penal2*jaccard(A, B);                     
else 
    qq = 1;
end                                

