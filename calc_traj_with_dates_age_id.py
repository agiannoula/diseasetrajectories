
#Disease trajectory clustering software
#Copyright (C) 2020 Alexia Giannoula and Laura I. Furlong, IBI group.
#This file is part of the Disease trajectory and clustering software

#The Disease trajectory and clustering software is free software: you can redistribute it and/or modify it under 
#the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 
#of the License, or (at your option) any later version (https://www.gnu.org/licenses/gpl-3.0.html).
 
#Disease trajectory and clustering software is distributed in the hope that it will be useful,but WITHOUT 
#ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
#See the GNU General Public License for more details.
 
#You should have received a copy of the GNU General Public License
#along with this program.  If not, see <http://www.gnu.org/licenses/>.
 
#How to cite Disease trajectory and clustering software:
#Giannoula, A., Centeno, E., Mayer M.-A., Sanz, F., & Furlong, L. I. A system-level analysis of patient disease 
#trajectories based on clinical, phenotypic and molecular similarities. Submitted to Bioinformatics.

import os, sys
import time

patient_file = sys.argv[1]
dates_file = sys.argv[2]
ages_file = sys.argv[3]
info_file = sys.argv[4]


fd = open(patient_file,"r")

all_patients = []
all_patients_sets = []
all_patients_indices = []

dates_fd = open(dates_file,"r")

all_patient_disease_dates = []

ages_fd = open(ages_file,"r")

all_patient_disease_ages = []

id_fd = open(info_file,"r")

all_patient_ids = []


line_num=1
for line in fd:
	
	patient_diseases = [ d for d in line.strip().split(" ") ]

	patient_diseases_uniq = []

	dates = dates_fd.readline().strip().split(" ")
	patient_dates = []

	ages = ages_fd.readline().strip().split(" ")
	patient_ages = []

	ids = id_fd.readline().strip().split(" ")
	patient_ids = ids[0]

	if len(patient_diseases)!=len(dates):
		print(line_num)
		print(patient_diseases)
		print(dates)
		print(ages)
	        print(ids)
		print("EXIT BECAUSE THE NUMBER OF DISEASES AND DATES DOES NOT MATCH!")
		sys.exit(1)
		#continue
	line_num+=1

	current_date_index = 0
	for d in patient_diseases:
		if d not in patient_diseases_uniq:
			patient_diseases_uniq.append(d)
			patient_dates.append(dates[current_date_index])
			patient_ages.append(ages[current_date_index])
		current_date_index+=1
	
	all_patients.append(patient_diseases_uniq)
	all_patients_sets.append(set(patient_diseases_uniq))
	all_patients_indices.append( dict( [(value, index) for index, value in enumerate(patient_diseases_uniq)] ) )

	all_patient_disease_dates.append(patient_dates)
	all_patient_disease_ages.append(patient_ages)
	all_patient_ids.append(patient_ids)
	
			
fd.close()
dates_fd.close()
ages_fd.close()
id_fd.close()

output_files = {}

itime = time.time()


for i in range(len(all_patients)):	

	if i%100==0:
		ctime = time.time()-itime

		percentage_done = i*100/float(len(all_patients))
		
		if i>0:
			expected_time = ctime*100/percentage_done-ctime
			sys.stderr.write("%s%% patients done! Expected time to finish: %s seconds\n" %(i*100/float(len(all_patients)), expected_time))

	for j in range(i+1, len(all_patients)):

		common_diseases = all_patients_sets[i].intersection(all_patients_sets[j])

		if len(common_diseases)<2:
			continue

		indexes_patient1 = []
		indexes_patient2 = []



		for common_disease in common_diseases:
			indexes_patient1.append( all_patients_indices[i][common_disease] )
			indexes_patient2.append( all_patients_indices[j][common_disease] )

		indexes_patient1_sorted = sorted(indexes_patient1)
		indexes_patient2_sorted = sorted(indexes_patient2)

		ranks1 = [ indexes_patient1_sorted.index(x) for x in indexes_patient1 ]
		ranks2 = [ indexes_patient2_sorted.index(x) for x in indexes_patient2 ]

		if ranks1 == ranks2:
			order = 1
		else:
			order = -1



		if len(common_diseases) not in output_files:
			output_files[len(common_diseases)]=open("traj_%s.txt" %len(common_diseases),"w")



		output_files[len(common_diseases)].write("%s\n" % "\t".join(map(str,[all_patient_ids[i],
										     all_patient_ids[j],
										     len(common_diseases),
										     #"\t".join(map(str,values_to_print)), 
										     "\t".join(map(str,[all_patients[i][k] for k in indexes_patient1_sorted])),
										     "\t".join(map(str,[all_patients[j][k] for k in indexes_patient2_sorted])),
										     "\t".join([all_patient_disease_dates[i][k] for k in indexes_patient1_sorted]), # dates for patient1
										     "\t".join([all_patient_disease_dates[j][k] for k in indexes_patient2_sorted]), # dates for patient2
										     "\t".join([all_patient_disease_ages[i][k] for k in indexes_patient1_sorted]), # ages for patient1
										     "\t".join([all_patient_disease_ages[j][k] for k in indexes_patient2_sorted]), # ages for patient2
										     order])))


		
for output_file in output_files.values():
	output_file.close()
				
		

