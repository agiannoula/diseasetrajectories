%Disease trajectory clustering software
%Copyright (C) 2020 Alexia Giannoula and Laura I. Furlong, IBI group.
%This file is part of the Disease trajectory and clustering software

%The Disease trajectory and clustering software is free software: you can redistribute it and/or modify it under 
%the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 
%of the License, or (at your option) any later version (https://www.gnu.org/licenses/gpl-3.0.html).
 
%Disease trajectory and clustering software is distributed in the hope that it will be useful,but WITHOUT 
%ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
%See the GNU General Public License for more details.
 
%You should have received a copy of the GNU General Public License
%along with this program.  If not, see <http://www.gnu.org/licenses/>.
 
%How to cite Disease trajectory and clustering software:
%Giannoula, A., Centeno, E., Mayer M.-A., Sanz, F., & Furlong, L. I. A system-level analysis of patient disease 
%trajectories based on clinical, phenotypic and molecular similarities. Submitted to Bioinformatics.

%%%% Create a map with the sets of annotated genes for all ICD-9 codes involved
%%%% in the study and another for the corresponding weights that will be
%%%% used to penalize the final genetic distance between two diseases, in
%%%% the case where a disease code is not associated with any annotated
%%%% gene. In this case, a substitute disease is sought by finding the most
%%%% semantically similar disease with at least one annotated gene.
% By Alexia Giannoula - alexia.giannoula@upf.edu

clear all

N = 2;
load t2
t = t2;
u2 = unique(table2array(t(:,1:N)));


N = 3;
load t3 
t = t3;
u3 = unique(table2array(t(:,1:N)));


N = 4;
load t4
t = t4;
u4 = unique(table2array(t(:,1:N)));


N = 5;
load t5
t = t5;
u5 = unique(table2array(t(:,1:N)));

N = 6;
load t6
t = t6;
u6 = unique(table2array(t(:,1:N)));

u =[u2; u3; u4; u5; u6];

u = cellstr(u);
u = unique(u);

L = size(u,1)



%setup DATABASES connection with MYSQL
%semantic similarity database
instance = input('Instance for database: ');
username = input('Username for database: ');
password = input('Password for database: ');

conn = database(instance, username, password);

%DisGeNET database
instanceGEN = input('Instance for DisGeNET database: ');
usernameGEN = input('Username for DisGeNET database: ');
passwordGEN = input('Password for DisGeNET database: ');
conn_gen = database(instanceGEN, usernameGEN, passwordGEN);

qu1 = "SELECT icd9cm_allsab_diso.cui FROM semantic_similarity.icd9cm_allsab_diso where icd9cm_allsab_diso.code regexp '(^| )";
qu3 = "($| )'";


%look for genes. first link CUI with disease ID (nid)
qu_g1a = "SELECT disease.nid from DisGeNET_2017.disease where disease.id = '";
qu_g1b = char(39);
%look for genes. second link disease nid with gene nid
qu_g2a = "SELECT distinct gene_disease.gene_nid from DisGeNET_2017.gene_disease where gene_disease.disease_nid = ";
qu_g2b = " and (source = 'UNIPROT' or source = 'CTD_human' or source = 'ORPHANET' or source = 'PSYGENET')";


qu_fin_a = "SELECT ytex_similarity_icd9cm_allsab_diso.sokal from semantic_similarity.ytex_similarity_icd9cm_allsab_diso where ytex_similarity_icd9cm_allsab_diso.cui1 = '";
qu_fin_b = "' AND ytex_similarity_icd9cm_allsab_diso.cui2 = '";

qu_allcuis = "SELECT * FROM semantic_similarity.icd9cm_allsab_diso";
res_allcuis = select(conn,qu_allcuis);


keyset = strings(1,L); 
altern_cui = strings(1,L); 
altern_title = strings(1,L);
altern_code = strings(1,L);

for i=1:L
    str = sprintf('L = %d out of %d',i,L);
    disp(str)
                  
    r = u{i};
    dot_r = strfind(r,'.');
    if (~isempty(dot_r))            
        Lr = length(r);
        r_beg = r(1:dot_r-1);
        r_end = r(dot_r+1:Lr);
        if (dot_r == 2)
            r_beg = strcat('00',r_beg);
        elseif (dot_r == 3)
            r_beg = strcat('0',r_beg);
        elseif (dot_r == 5)   %% included only because Vcodes where substituted with 10. next, delete.
            r_beg = strcat('V',r_beg(3:4));
        end       
        rr = strcat(strcat(r_beg,'\\.'),r_end);
    else
        if (length(r) == 1)
            rr = strcat('00',r);
        elseif (length(r) == 2)
            rr = strcat('0',r);
        else
            rr = r;
        end            
    end        

    query_r=strcat(strcat(qu1,rr),qu3);               
    res_r=select(conn,query_r);

    if (isempty(res_r) ~= 1)

        query_g1 = strcat(strcat(qu_g1a,res_r.cui),qu_g1b);        
        res_g1 = select(conn_gen,query_g1);   
        
        if isempty(res_g1)                
            disp('empty result from DisGeNET query-1: no disease nid found...');            
            flag(i) = -2;
        else
            query_g2 = strcat(strcat(qu_g2a,string(res_g1.nid)),qu_g2b);        
            res_g2 = select(conn_gen,query_g2);              
            
            if isempty(res_g2)
                disp('no description from query-2: nid found, but no genes....')
                flag(i) = -2;
            else                           
                gene_nid =res_g2.gene_nid;            
                flag(i) = 1;
                gene_descr = "cui, genes found for the code"
                gene_weight(i) = 2;
                gene_num(i) = 0;                        
            end
        end 
        
        %in case of codes/cuis without gene description
        if (flag(i) == -2)
            disp(flag(i))
                       
            for k=1:size(res_allcuis,1)             
                if (strcmp(res_r.cui,res_allcuis.cui(k)) ~= 1)                    
                   query_list = strcat(strcat(strcat(qu_fin_a,res_r.cui),strcat(qu_fin_b,res_allcuis.cui(k))),qu_g1b);        
                   res_list = select(conn,query_list);            
                   dist(k) =res_list.sokal;                                        
                end                 
             end
             t_allcuis = res_allcuis;
             t_allcuis.dist = dist';
             t_allcuis = sortrows(t_allcuis,-4);
             m = 1;
             res_loop2 = [];
             while isempty(res_loop2)                 
                 query_loop1 = strcat(strcat(qu_g1a,t_allcuis.cui(m)),qu_g1b);        
                 res_loop1 = select(conn_gen,query_loop1);
                 if ~isempty(res_loop1)                                                        
                    query_loop2 = strcat(strcat(qu_g2a,string(res_loop1.nid)),qu_g2b);        
                    res_loop2 = select(conn_gen,query_loop2);              
                 end                                                                              
                 m = m+1;
             end             
             sel = m-1;           
             if (sel <= size(t_allcuis,1))
                 disp([sel t_allcuis.dist(sel)])
                 flag(i) = 2;
                 gene_nid =res_loop2.gene_nid;    
                 gene_descr = "cui,genes found, after searching for sem-sim";
                 gene_weight(i) = t_allcuis.dist(sel);
                 gene_num(i) = sel;
                 altern_cui(i) = t_allcuis.cui(sel);
                 altern_title(i) = t_allcuis.title(sel);
                 altern_code(i) = t_allcuis.code(sel);
             else                
                gene_nid = '-2';
                gene_descr = 'cui,no-genes even after searching entire database for semantic similarity...';
                gene_weight(i) = -2;
                gene_num(i) = -2;
             end
                 
             clear dist 
        end
                                                                                                                
    elseif (isempty(res_r))                            
               
        query_r=strcat(strcat(qu1,r_beg),qu3);    %check 3dig level          
        res_r2=select(conn,query_r);
                
        if (isempty(res_r2) ~= 1)
            query_g1 = strcat(strcat(qu_g1a,res_r2.cui),qu_g1b);        
            res_g1 = select(conn_gen,query_g1);           
            
            if isempty(res_g1)                
                disp('3-dig: empty result from DisGeNET query-1: no disease nid found...');            
                flag(i) = -3;
            else
                query_g2 = strcat(strcat(qu_g2a,string(res_g1.nid)),qu_g2b);        
                res_g2 = select(conn_gen,query_g2);              
            
                if isempty(res_g2)
                    disp('3-dig: empty result from DisGeNET query-2: nid found, but no genes....')
                    flag(3) = -2;
                else                           
                    gene_nid =res_g2.gene_nid;            
                    flag(i) = 3;
                    gene_descr = "3-dig:, genes found for the 3-dig code"
                    gene_weight(i) = 3;
                    gene_num(i) = -3;                        
                end
            end               
            
            %in case of codes/cuis without phenotype
            if (flag(i) == -3)
                disp(flag(i))
                
                for k=1:size(res_allcuis,1)             
                    if (strcmp(res_r2.cui,res_allcuis.cui(k)) ~= 1)                    
                        query_list = strcat(strcat(strcat(qu_fin_a,res_r2.cui),strcat(qu_fin_b,res_allcuis.cui(k))),qu_g1b);        
                        res_list = select(conn,query_list);                                
                        dist2(k) =res_list.sokal;                                        
                    end                 
                end
                t_allcuis = res_allcuis;
                t_allcuis.dist = dist2';
                t_allcuis = sortrows(t_allcuis,-4);
                m = 1;
                res_loop2 = [];
                while isempty(res_loop2)                 
                    query_loop1 = strcat(strcat(qu_g1a,t_allcuis.cui(m)),qu_g1b);        
                    res_loop1 = select(conn_gen,query_loop1);
                    if ~isempty(res_loop1)                                                            
                        query_loop2 = strcat(strcat(qu_g2a,string(res_loop1.nid)),qu_g2b);        
                        res_loop2 = select(conn_gen,query_loop2);              
                    end                                                                              
                    m = m+1;
                end                                                            
                %                
                sel = m-1;
                if (sel <= size(t_allcuis,1))
                    disp([sel t_allcuis.dist(sel)])
                    flag(i) = 4;
                    gene_nid =res_loop2.gene_id;
                    gene_descr = "no cui,genes found for 3-dig, after searching for sem-sim";    
                    gene_weight(i) = t_allcuis.dist(sel);
                    gene_num(i) = sel;
                    altern_cui(i) = t_allcuis.cui(sel);
                    altern_title(i) = t_allcuis.title(sel);
                    altern_code(i) = t_allcuis.code(sel);                                        
                else                
                    gene_nid = '-3';
                    gene_descr = '3-dig,no genes even after searching entire database for semantic similarity...';
                    gene_weight(i) = -3;
                    gene_num(i) = -4;
                end
                clear dist 
            end
                        
        else                 
            disp(r_beg)
            input('STILL empty result from semantic query: no CUI for 5 dig nor 3 dig...')     
            gene_nid = -1;
            gene_descr = 'no-cui,no-genes';
            flag(i) = -1;                
            gene_weight(i) = -1;
            gene_num(i) = -1;
        end
        
    else
        input('problem...')
    end    

    keyset(i) = r;       
    valueset_id{i} = gene_nid;
    valueset_descr{i} = gene_descr;            
    
    
end

gen_flag = flag;

map_gen_id = containers.Map(cellstr(keyset),valueset_id);
map_gen_descr = containers.Map(cellstr(keyset),valueset_descr);
map_gen_dist = containers.Map(cellstr(keyset),gene_weight);
map_gen_order = containers.Map(cellstr(keyset),gene_num);

save map_gen_id map_gen_id 
save map_gen_dist map_gen_dist

close(conn);
close(conn_gen);



