
%Disease trajectory clustering software
%Copyright (C) 2020 Alexia Giannoula and Laura I. Furlong, IBI group.
%This file is part of the Disease trajectory and clustering software

%The Disease trajectory and clustering software is free software: you can redistribute it and/or modify it under 
%the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 
%of the License, or (at your option) any later version (https://www.gnu.org/licenses/gpl-3.0.html).
 
%Disease trajectory and clustering software is distributed in the hope that it will be useful,but WITHOUT 
%ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
%See the GNU General Public License for more details.
 
%You should have received a copy of the GNU General Public License
%along with this program.  If not, see <http://www.gnu.org/licenses/>.
 
%How to cite Disease trajectory and clustering software:
%Giannoula, A., Centeno, E., Mayer M.-A., Sanz, F., & Furlong, L. I. A system-level analysis of patient disease 
%trajectories based on clinical, phenotypic and molecular similarities. Submitted to Bioinformatics.

%%%% cluster based on Phenotypic Similarity


clear all

penalflag = 1;

N = 2;
load t2
t = t2;
u2 = table2array(t(:,1:N));
pat2 = t.pat;
c12_2 = t.c12;

N = 3;
load t3 
t = t3;
u3 = table2array(t(:,1:N));
pat3 = t.pat;
c12_3 = t.dt1;
c13_3 = t.dt2;

N = 4;
load t4
t = t4;
u4 = table2array(t(:,1:N));
pat4 = t.pat;
c12_4 = t.dt1;
c13_4 = t.dt2;
c14_4 = t.dt3;

N = 5;
load t5
t = t5;
u5 = table2array(t(:,1:N));
pat5 = t.pat;
c12_5 = t.dt1;
c13_5 = t.dt2;
c14_5 = t.dt3;
c15_5 = t.dt4;

N = 6;
load t6
t = t6;
u6 = table2array(t(:,1:N));
pat6 = t.pat;
c12_6 = t.dt1;
c13_6 = t.dt2;
c14_6 = t.dt3;
c15_6 = t.dt4;
c16_6 = t.dt5;

L2 = size(u2,1);
L3 = size(u3,1);
L4 = size(u4,1);
L5 = size(u5,1);
L6 = size(u6,1);

u = {};
for i=1:L2
    u{i}=(u2(i,:));
end
for i=1:L3
    u{L2+i}=(u3(i,:));
end
for i=1:L4
    u{L2+L3+i}=(u4(i,:));    
end
for i=1:L5
    u{L2+L3+L4+i}=(u5(i,:));    
end
for i=1:L6
    u{L2+L3+L4+L5+i}=(u6(i,:));    
end


%L = length total
disp('total #trajectories:')
L = L2+L3+L4+L5+L6

data = u;



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%% classification with DTW 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear Q dclust cid
disp('*******************************')
disp('chosen THRESHOLD:')
thres = input('threshold = ');
disp('*******************************')

str_thres = num2str(thres);

sfile = sprintf('Q_%s_PHEM.txt',str_thres);
sfile_info = sprintf('Q_%s_PHEM_info.txt', str_thres);
sfile_info_sort = sprintf('Q_%s_PHEM_info_srt.txt', str_thres);
Qmat = sprintf('Q_%s_PHEM.mat',str_thres);
CIDmat = sprintf('CID_%s_PHEM.mat',str_thres);



Q{1}{1} = data{1};

cid = zeros(L,1);
cid(1) = 1;


load map_dis_phen


cc = 1;
for i=2:L   
    if (mod(i,50) == 0)
        sp = sprintf('progress: %.1f%%',i/L*100);
        disp(sp)
    end
    s = data{i};
    [dim,C] = size(Q);
    for nc=1:C
        [dim,sc]=size(Q{nc});
        d=0;
        for zc=1:sc                        
            t = Q{nc}{zc};                 
            [Dist,D,k,w,rw,tw]=dtw_disPHEN_map(s,t,map_dis_phen);
            d=d+Dist;            
        end
        dclust(nc) = d/sc;
    end
    [m(cc),ind]=min(dclust);        
    if (m(cc)<thres)        
        Q{ind}{end+1}=[s];
        cid(cc) = ind;
    else        
        Q{nc+1}={s};
        cid(cc) = nc+1;
    end       
    cc = cc + 1;
              
    clear dclust s    
end

save(Qmat,'Q');
save(CIDmat,'cid');
save m m

[dim,nclust] = size(Q);

disp('saved Q , cid....')
%break;

disp('Saving clusters into a file.....'); 


fid = fopen(sfile,'w');
fid2 = fopen(sfile_info,'w');

trajMin = 3;

tclust = 0;
tot = 0;
pin = [];
for i = 1:nclust    
    clust = find(cid == i); 
    
    tot1 = 0;
    if (length(clust) >= trajMin)
    %if (length(clust) <=9 & length(clust)>5)    
        tclust = tclust + 1;
        str = sprintf('Cluster%d (%d):',tclust,length(clust));    
        fprintf(fid,'%s\n',str);        
        
        for k = 1:length(clust)
            
            S = size(data{clust(k)},2);
            if (S == 6)
                patients = pat6(clust(k)-L2-L3-L4-L5);
                times = [c12_6(clust(k)-L2-L3-L4-L5) c13_6(clust(k)-L2-L3-L4-L5) c14_6(clust(k)-L2-L3-L4-L5) c15_6(clust(k)-L2-L3-L4-L5) c16_6(clust(k)-L2-L3-L4-L5)];            
            elseif (S == 5)
                patients = pat5(clust(k)-L2-L3-L4);
                times = [c12_5(clust(k)-L2-L3-L4) c13_5(clust(k)-L2-L3-L4) c14_5(clust(k)-L2-L3-L4) c15_5(clust(k)-L2-L3-L4)];
            elseif (S == 4)
                patients = pat4(clust(k)-L2-L3);
                times = [c12_4(clust(k)-L2-L3) c13_4(clust(k)-L2-L3) c14_4(clust(k)-L2-L3)];
            elseif (S == 3)
                patients = pat3(clust(k)-L2);
                times = [c12_3(clust(k)-L2) c13_3(clust(k)-L2)];
            elseif (S == 2)
                patients = pat2(clust(k));
                times = [c12_2(clust(k))];
            end    
            tot1 = tot1 + patients;    
            
            %disp([Q{i}{k}])
            ss = size(Q{i}{k},2);
            
            for s=1:ss            
                fprintf(fid,'%s ',char(Q{i}{k}(s))); 
            end
            fprintf(fid,' -> ');
            fprintf(fid,'%d ',patients); fprintf(fid,' -> ');
            fprintf(fid,'%.2f ', times);            
            fprintf(fid,'\n');            
          
        end                         
        fprintf(fid,'\n');
        pin = [pin tot1];
        
        str = sprintf('%d\t%d\t%d',tclust,length(clust),tot1);   
        fprintf(fid2,'%s',str); fprintf(fid2,'\n'); 
       
    end
    
    tot = tot + tot1/length(clust);
    
end

disp('average # patients:')
disp(tot/tclust)


fclose(fid); fclose(fid2);


t = readtable(sfile_info,'delimiter','\t','format','%d %d %d');
t2 = sortrows(t,-2);
writetable(t2,sfile_info_sort,'delimiter','\t');





