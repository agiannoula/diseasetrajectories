function y = BMA_phen(A, B, map_phen_sok)

%Disease trajectory clustering software
%Copyright (C) 2020 Alexia Giannoula and Laura I. Furlong, IBI group.
%This file is part of the Disease trajectory and clustering software

%The Disease trajectory and clustering software is free software: you can redistribute it and/or modify it under 
%the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 
%of the License, or (at your option) any later version (https://www.gnu.org/licenses/gpl-3.0.html).
 
%Disease trajectory and clustering software is distributed in the hope that it will be useful,but WITHOUT 
%ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
%See the GNU General Public License for more details.
 
%You should have received a copy of the GNU General Public License
%along with this program.  If not, see <http://www.gnu.org/licenses/>.
 
%How to cite Disease trajectory and clustering software:
%Giannoula, A., Centeno, E., Mayer M.-A., Sanz, F., & Furlong, L. I. A system-level analysis of patient disease 
%trajectories based on clinical, phenotypic and molecular similarities. Submitted to Bioinformatics.

[row,M]=size(A); if (row > M) M=row; A=A'; end;
[row,N]=size(B); if (row > N) N=row; B=B'; end;

% first summation
for i=1:M
        
    r = strcat('HP:',A{i});
    dd = [];
    for j=1:N
        t = strcat('HP:',B{j});
        
        cmp_r_t = strcmp(r,t);
        
        if (cmp_r_t == 0)
            strkey = strcat(strcat(r,'_'),t);
            qq = map_phen_sok(strkey);
        else
            qq = 1;
        end
        dd=[dd qq];
    end
    max_di(i) = max(dd);    
end
clear dd

% second summation
for j=1:N        
    t = strcat('HP:',B{j});
    dd2 = [];
    for i=1:M
        r = strcat('HP:',A{i});
        
        cmp_r_t = strcmp(r,t);
        
        if (cmp_r_t == 0)
            strkey = strcat(strcat(r,'_'),t);
            qq = map_phen_sok(strkey);
        else
            qq = 1;
        end
        dd2=[dd2 qq];
                
    end
    max_dj(j) = max(dd2);
end
clear dd2


mi = max_di(max_di~=-1);   
mj = max_dj(max_dj~=-1);

y = (sum(mi)+sum(mj))/(size(mi,2)+size(mj,2));


