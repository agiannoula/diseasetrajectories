function [Distance,V,k,w,rw,tw]=dtw_GEN_map(r,t,map_gen,map_dist,penalflag)

%Disease trajectory clustering software
%Copyright (C) 2020 Alexia Giannoula and Laura I. Furlong, IBI group.
%This file is part of the Disease trajectory and clustering software

%The Disease trajectory and clustering software is free software: you can redistribute it and/or modify it under 
%the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 
%of the License, or (at your option) any later version (https://www.gnu.org/licenses/gpl-3.0.html).
 
%Disease trajectory and clustering software is distributed in the hope that it will be useful,but WITHOUT 
%ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
%See the GNU General Public License for more details.
 
%You should have received a copy of the GNU General Public License
%along with this program.  If not, see <http://www.gnu.org/licenses/>.
 
%How to cite Disease trajectory and clustering software:
%Giannoula, A., Centeno, E., Mayer M.-A., Sanz, F., & Furlong, L. I. A system-level analysis of patient disease 
%trajectories based on clinical, phenotypic and molecular similarities. Submitted to Bioinformatics.


% Dynamic Time Warping Algorithm
% Distance is the distance between trajectories t and r



[row,M]=size(r); if (row > M) M=row; r=r'; end;
[row,N]=size(t); if (row > N) N=row; t=t'; end;

jaccard = @(A, B) numel(intersect(A, B))./numel(union(A, B));

for i = 1:M
    for j = 1:N    
        
        cmp_r_t = strcmp(r{i},t{j});
        
        if (cmp_r_t == 0)
            A = map_gen(r{i}); 
            B = map_gen(t{j}); 

            penal1 = 1; 
            penal2 = 1;
            if (penalflag == 1)
                w1 = map_dist(r{i}); 
                w2 = map_dist(t{j});
                if (w1>0 & w1 <1)
                    penal1 = w1;        
                end
                if (w2>0 & w2 <1)
                    penal2 = w2;        
                end
            end        
            qq = penal1*penal2*jaccard(A, B);              
        else
            qq= 1;
        end
        d(i,j) = 1-qq;
    end    
end


V=zeros(size(d));
V(1,1)=d(1,1);

for m=2:M
    V(m,1)=d(m,1)+V(m-1,1);
end
for n=2:N
    V(1,n)=d(1,n)+V(1,n-1);
end
for m=2:M
    for n=2:N
        V(m,n)=d(m,n)+min(V(m-1,n),min(V(m-1,n-1),V(m,n-1))); 
    end
end

Distance=V(M,N);
n=N;
m=M;
k=1;
w=[M N];
while ((n+m)~=2)
    if (n-1)==0
        m=m-1;
    elseif (m-1)==0
        n=n-1;
    else 
      [values,number]=min([V(m-1,n),V(m,n-1),V(m-1,n-1)]);
      switch number
      case 1
        m=m-1;
      case 2
        n=n-1;
      case 3
        m=m-1;
        n=n-1;
      end
  end
    k=k+1;
    w=[m n; w]; 
end

% warped sequences
rw=r(w(:,1));
tw=t(w(:,2));

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
