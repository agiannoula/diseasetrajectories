%Disease trajectory clustering software
%Copyright (C) 2020 Alexia Giannoula and Laura I. Furlong, IBI group.
%This file is part of the Disease trajectory and clustering software

%The Disease trajectory and clustering software is free software: you can redistribute it and/or modify it under 
%the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 
%of the License, or (at your option) any later version (https://www.gnu.org/licenses/gpl-3.0.html).
 
%Disease trajectory and clustering software is distributed in the hope that it will be useful,but WITHOUT 
%ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
%See the GNU General Public License for more details.
 
%You should have received a copy of the GNU General Public License
%along with this program.  If not, see <http://www.gnu.org/licenses/>.
 
%How to cite Disease trajectory and clustering software:
%Giannoula, A., Centeno, E., Mayer M.-A., Sanz, F., & Furlong, L. I. A system-level analysis of patient disease 
%trajectories based on clinical, phenotypic and molecular similarities. Submitted to Bioinformatics.


%horizontal and vertical-wise evaluation of clusters for a specific
%threshold.


clear all

thres = input('Threshold: ');

str_thres = num2str(thres);

penalflag = 1;

Qmat = sprintf('Q_%s_SEM.mat',str_thres); %or Q_%s_GEN.mat, Q_%s_PHEN.mat
Qdirmat = sprintf('Q_%s_SEM_dir.mat',str_thres);
CIDmat = sprintf('CID_%s_SEM.mat',str_thres);
sfile = sprintf('Q_%s_%s.txt',str_thres);

evalfile = sprintf('eval_%s_SEM.txt',str_thres);
evalfile_rows = sprintf('eval_%s_%s_rows.csv',str_thres,'SEM');    
evalfile_rows_dir = sprintf('eval_%s_%s_rows_dir.csv',str_thres,'SEM');    

ev=readtable(evalfile_rows,'delimiter','\t','format','%d %f %f %f');
cl = ev.cluster;

dir_file = 'diseases2.txt';
%format of the trajectory file length 2: dis1 dis2 #pat Dt age1 age2
t=readtable(dir_file,'delimiter','\t','format','%s %s %d %f %f %f');

      
load(Qmat)
load(CIDmat)
[dim,nclust_orig] = size(Q);

%clinical (semantic)
load map_sem_sok
%genetic
load map_gen_id
load map_gen_dist
%phenotypic
load map_dis_phen
map_dis_hpo = map_dis_phen;


PC = '185'; %prostate cancer ICD-9 code


del_ent = 0;
del_part = 0;
Qdir = {};
rr = 1;
trajMin = 3;

disp('Check clusters for directionality...');

clust_kept = [];
clust_new = [];
for i=1:length(cl)
    str=sprintf('Cluster %d\n',cl(i)); disp(str)
         
    clust = find(cid == cl(i));     
   
    del = 0;  
    qq = 1;
    for k=1:length(clust)  
       
        traj = Q{cl(i)}{k};
         [a,b]=ismember(PC,traj);
        if (b>=2 & b<length(traj))
            for x=1:b-1
                dis = traj(x);
                discomp = ismember(t.dis2,dis);
                idis = find(discomp == 1);
                dir = t.dir(idis); 
                if (dir == 1)
                    Q{cl(i)}{k} = [];                                  
                end
            end
            
            for y=b+1:length(traj)
                dis = traj(y);
                discomp = ismember(t.dis2,dis);
                idis = find(discomp == 1);
                dir = t.dir(idis); 
                if (dir == -1)
                    Q{cl(i)}{k} = [];                                                   
                end
            end
        elseif (b == 1)
            for y=b+1:length(traj)
                dis = traj(y);
                discomp = ismember(t.dis2,dis);
                idis = find(discomp == 1);
                dir = t.dir(idis); 
                if (dir == -1)
                    Q{cl(i)}{k} = [];                                  
                end
            end
        elseif (b == length(traj))
            for x=1:b-1
                dis = traj(x);
                discomp = ismember(t.dis2,dis);
                idis = find(discomp == 1);
                dir = t.dir(idis); 
                if (dir == 1)
                    Q{cl(i)}{k} = [];                                  
                end
            end
        end  
                                       
        ss = size(Q{cl(i)}{k},2);
        if (ss >= 1)
            Qdir{rr}{qq} = Q{cl(i)}{k};
            qq = qq+1;            
        else
            del = del + 1;
        end         
        
    end
    
    if (length(clust)-del >= trajMin)   
        clust_kept(rr) = cl(i);
        clust_new(rr) = rr;
        rr = rr+1;
    end
            
    if (del == length(clust))
        del_ent = del_ent+1;                  
    elseif (del < length(clust) & del > 0)
        del_part = del_part + 1;                       
    elseif (del > length(clust))
        input('problem...');
    end
            
   
end


str = sprintf('Deleted entirely: %d from %d clusters',del_ent,length(cl)); disp(str)
str = sprintf('Deleted partially: %d from %d clusters',del_part,length(cl)); disp(str)

disp('New clusterization after directionality analysis...')
size(Qdir)

save(Qdirmat,'Qdir');


%%%%% evaluate cluster vertically
disp('Evaluate clusters vertically....')
Q = Qdir;

[dim,nclust] = size(Q);
tclust = 0;
Nuni = 1; Nu_mix = 1; Nmix = 1;

for i = 1:nclust     
    
    LC = size(Q{i},2);
    
    if (LC >= trajMin)
        tclust = tclust + 1;
          
        ss = zeros(1,LC);
        for k=1:LC            
            ss(k) = size(Q{i}{k},2);                                  
        end
        [u,ia,ic]=unique(ss);
        
        NL = length(u);
        minL = min(u); maxL = max(u);        
        Qtraj = cell(LC,maxL);
        for n = 1:NL
            uind = find(ss == u(n));
            for j=1:length(uind)
                for r = 1:u(n)
                    Qtraj(uind(j),r) = Q{i}{uind(j)}(r);
                end
                for r=u(n)+1:maxL
                    Qtraj(uind(j),r) = cellstr('X');
                end
            end                        
        end           
                   
        %eval cluster               
        Xq = strcmp(Qtraj,'X');
        xs = [];
        for r=1:size(Qtraj,1)
            xs = [xs find(Xq(r,:)==1)];
        end
        
        
        if isempty(xs)
            Rows = size(Qtraj,1); 
            Cols = size(Qtraj,2);
            for n = 1:Cols
                assoc = 0; c = 0; 
                for x=1:Rows  
                    s = Qtraj{x,n};
                    for m = x+1:Rows
                        t = Qtraj{m,n}; 
                        qq = eval_dis_semdist(s,t,map_sem_sok);
                        assoc = assoc + qq;
                        c = c + 1;
                    end                                                        
                end  
                assoc_uni{Nuni,1}= clust_kept(i);
                assoc_uni{Nuni,1+n}  = assoc / c;                  
            end                  
            Nuni = Nuni + 1;
        else
            uind = find(ss == minL);
            Qmin = Qtraj(uind,1:minL);
            Rows = size(Qmin,1);         
            Cols = size(Qmin,2);
            if (Rows > 1)
                for n = 1:Cols
                    assoc = 0; c = 0; 
                    for x=1:Rows  
                        s = Qmin{x,n};
                        for m = x+1:Rows
                            t = Qmin{m,n}; 
                            qq = eval_dis_semdist(s,t,map_sem_sok);
                            assoc = assoc + qq;
                            c = c + 1;
                        end                                                        
                    end  
                    assoc_uni_mix{Nu_mix,1}= clust_kept(i);
                    assoc_uni_mix{Nu_mix,1+n}  = assoc / c;                      
                end                 
                Nu_mix = Nu_mix + 1;
            end
            
            Qall = Qmin;
            for w = 2:NL               
                uind = find(ss == u(w));
                assoc_mix_init = -1 * ones(length(uind),u(w),minL);
                for j=1:length(uind)                      
                    for r = 1:u(w) 
                        s = Qtraj{ia(w)+j-1,r};
                        for n=1:minL
                            assoc = 0; c = 0;
                            for tj=1:Rows
                                t = Qmin{tj,n};   
                                qq = eval_dis_semdist(s,t,map_sem_sok);                            
                                assoc = assoc + qq;
                                c = c + 1;
                            end                            
                            assoc_mix_init(j,r,n)  = assoc / c;                            
                        end
                    end
                end   
                
                Qpatt=cell(length(uind),minL);
                for j=1:length(uind)
                    for n=1:minL
                        [ma,ka] = max(assoc_mix_init(j,:,n));
                        Qpatt{j,n} = Qtraj{ia(w)+j-1,ka};                        
                    end
                end                    
                
                Qall = [Qall; Qpatt];                
            end                                                                                                                                     
            Rows = size(Qall,1); 
            Cols = size(Qall,2);
            for n = 1:Cols
                assoc = 0; c = 0; 
                for x=1:Rows  
                    s = Qall{x,n};
                    for m = x+1:Rows
                        t = Qall{m,n}; 
                        qq = eval_dis_semdist(s,t,map_sem_sok);
                        assoc = assoc + qq;
                        c = c + 1;
                    end                                                        
                end  
                assoc_mix{Nmix,1} = clust_kept(i);
                assoc_mix{Nmix,1+n}  = assoc / c;                      
            end                      
            Nmix = Nmix + 1;
                             
        end
                                  
    end
    
    clear ss 
    
    
end


assoc_uni
[r,c] = size(assoc_uni);
k = 1;
for i=2:c
    ave_uni(k) = mean(cell2mat(assoc_uni(:,i)));
    k = k + 1;
end
ave_tot_uni = mean(ave_uni)


assoc_uni_mix
[r,c] = size(assoc_uni_mix);
k = 1;
for i=2:c
    ave_uni_mix(k) = mean(cell2mat(assoc_uni_mix(:,i)));
    k = k + 1;
end
ave_tot_uni_mix = mean(ave_uni_mix)


assoc_mix
[r,c] = size(assoc_mix);
k = 1;
for i=2:c
    ave_mix(k) = mean(cell2mat(assoc_mix(:,i)));
    k = k + 1;
end
ave_tot_mix = mean(ave_mix)        
        
eval_tot = mean([ave_tot_uni ave_tot_mix])
  

fid = fopen(evalfile,'w');
str = sprintf('SEM: Thres = %s -> %d clusters out of %d with >= %d traj with directionality (tot: %d)',str_thres,tclust,length(cl),trajMin,nclust_orig);
fprintf(fid,'%s\n',str);  
fprintf(fid,'---------\n');
fprintf(fid,'%d clusters with equal-length trajectories (assoc_uni)\n',size(assoc_uni,1));
fprintf(fid,'%d clusters with mixed-length trajectories (assoc_mix)\n',size(assoc_mix,1));
fprintf(fid,'%d clusters with mixed-length trajectories (assoc_uni_mix)\n\n',size(assoc_uni_mix,1));
fprintf(fid,'%.2f ',ave_uni); fprintf(fid,' (AVE_uni) --> '); fprintf(fid,' %.2f',ave_tot_uni); 
fprintf(fid,'\n');
fprintf(fid,'%.2f ',ave_mix); fprintf(fid,'  (AVE_mix) --> '); fprintf(fid,' %.2f',ave_tot_mix); 
fprintf(fid,'\n');
fprintf(fid,'%.2f ',ave_uni_mix); fprintf(fid,' (AVE_unimix) --> '); fprintf(fid,' %.2f',ave_tot_uni_mix); 
fprintf(fid,'\n\n');
fprintf(fid,'TOT (uni+mix) = '); fprintf(fid,'%.2f\n',eval_tot);  
fprintf(fid,'\n--------------------\n');
fprintf(fid,'UNI\n');
fprintf(fid,'---------\n');
[r,c] = size(assoc_uni);
for i=1:r
    fprintf(fid,'(%d): ',cell2mat(assoc_uni(i,1)));
    fprintf(fid,'%.2f ',cell2mat(assoc_uni(i,2:end)));
    fprintf(fid,'\n');
end
fprintf(fid,'\n\n');
fprintf(fid,'UNI_MIX\n');
fprintf(fid,'---------\n');
[r,c] = size(assoc_uni_mix);
for i=1:r
    fprintf(fid,'(%d): ',cell2mat(assoc_uni_mix(i,1)));
    fprintf(fid,'%.2f ',cell2mat(assoc_uni_mix(i,2:end)));
    fprintf(fid,'\n');
end
fprintf(fid,'\n\n');
fprintf(fid,'MIX\n');
fprintf(fid,'---------\n');
[r,c] = size(assoc_mix);
for i=1:r
    fprintf(fid,'(%d): ',cell2mat(assoc_mix(i,1)));
    fprintf(fid,'%.2f ',cell2mat(assoc_mix(i,2:end)));
    fprintf(fid,'\n');
end

fclose(fid);

%%%% evaluate clusters horizontally: 
tclust = 0;
for i = 1:nclust     
    
    LC = size(Q{i},2);
           
    if (LC >= trajMin)
        tclust = tclust + 1;
          
        ss = zeros(1,LC);
        for k=1:LC            
            ss(k) = size(Q{i}{k},2);                                  
        end
        [u,ia,ic]=unique(ss);
        
        NL = length(u);
        minL = min(u); maxL = max(u);        
        Qtraj = cell(LC,maxL);
        for n = 1:NL
            uind = find(ss == u(n));
            for j=1:length(uind)
                for r = 1:u(n)
                    Qtraj(uind(j),r) = Q{i}{uind(j)}(r);
                end
                for r=u(n)+1:maxL
                    Qtraj(uind(j),r) = cellstr('X');
                end
            end                        
        end           
                   
        %eval cluster               
        Xq = strcmp(Qtraj,'X');
        xs = [];
        for r=1:size(Qtraj,1)
            xs = [xs find(Xq(r,:)==1)];
        end
        
        
        if isempty(xs)
            Rows = size(Qtraj,1); 
            Cols = size(Qtraj,2);   
            eval_rows_g = zeros(1,Rows);
            eval_rows_ph = zeros(1,Rows);
            eval_rows_s = zeros(1,Rows);
            eval_rows_comb = zeros(1,Rows);

            for x = 1:Rows
                assoc_g = 0; c_g = 0; 
                assoc_ph = 0; c_ph = 0; 
                assoc_s = 0; c_s = 0; 
                assoc_comb = 0; c_comb = 0;
                for n=1:Cols                      
                    s = Qtraj{x,n};
                    for m = n+1:Cols                        
                        if (n~=m)                              
                            t = Qtraj{x,m};                                 
                            [qq1,qq2,qq3] = eval_rows_all(s,t,map_gen_id,map_gen_dist,map_dis_hpo,map_sem_sok,penalflag);                            
                            assoc_g = assoc_g + qq1;
                            assoc_ph = assoc_ph + qq2;
                            assoc_s = assoc_s + qq3;    
                            assoc_comb = assoc_comb + (qq1+qq2+qq3)/3;
                            c_g = c_g + 1;
                            c_ph = c_ph + 1;
                            c_s = c_s + 1;
                            c_comb = c_comb + 1;
                        end
                    end                                                        
                    eval_rows_g(x)= assoc_g/c_g;
                    eval_rows_ph(x)= assoc_ph/c_ph;
                    eval_rows_s(x)= assoc_s/c_s;  
                    eval_rows_comb(x)= assoc_comb/c_comb;                    
                end                
            end
            tot_rows_ph{tclust,1}= i;
            tot_rows_ph{tclust,2}= mean(eval_rows_ph);             
            tot_rows_g{tclust,1}= i;
            tot_rows_g{tclust,2}= mean(eval_rows_g);     
            tot_rows_s{tclust,1}= i;
            tot_rows_s{tclust,2}= mean(eval_rows_s);                         
            tot_rows_comb{tclust,1}= i;
            tot_rows_comb{tclust,2}= mean(eval_rows_comb);                         
        else
            uind = find(ss == minL);
            Qmin = Qtraj(uind,1:minL);
            Rows = size(Qmin,1);         
            Cols = size(Qmin,2);           
            Qall = Qmin;
            for w = 2:NL               
                uind = find(ss == u(w));
                assoc_mix_init = -1 * ones(length(uind),u(w),minL);
                for j=1:length(uind)  
                    for r = 1:u(w) 
                        s = Qtraj{ia(w)+j-1,r};
                        for n=1:minL
                            assoc = 0; c = 0;
                            for tj=1:Rows
                                t = Qmin{tj,n};                                   
                                qq = eval_dis_semdist(s,t,map_sem_sok);
                                assoc = assoc + qq;
                                c = c + 1;
                            end                            
                            assoc_mix_init(j,r,n)  = assoc / c;                            
                        end
                    end
                end                   
                Qpatt=cell(length(uind),minL);
                for j=1:length(uind)
                    for n=1:minL
                        [ma,ka] = max(assoc_mix_init(j,:,n));
                        Qpatt{j,n} = Qtraj{ia(w)+j-1,ka};                        
                    end
                end
                
                Qall = [Qall; Qpatt];                
            end                                                                                                                                     
            Rows = size(Qall,1); 
            Cols = size(Qall,2);
            eval_rows_g = zeros(1,Rows);
            eval_rows_ph = zeros(1,Rows);
            eval_rows_s = zeros(1,Rows);
            eval_rows_comb = zeros(1,Rows);
            for x = 1:Rows
                assoc_g = 0; c_g = 0; 
                assoc_ph = 0; c_ph = 0; 
                assoc_s = 0; c_s = 0; 
                assoc_comb = 0; c_comb = 0;
                for n=1:Cols                      
                    s = Qall{x,n};
                    for m = n+1:Cols                        
                        if (n~=m)                               
                            t = Qall{x,m};                                 
                            [qq1,qq2,qq3] = eval_rows_all(s,t,map_gen_id,map_gen_dist,map_dis_hpo,map_sem_sok,penalflag);                            
                            assoc_g = assoc_g + qq1;
                            assoc_ph = assoc_ph + qq2;
                            assoc_s = assoc_s + qq3; 
                            assoc_comb = assoc_comb + (qq1+qq2+qq3)/3;
                            c_g = c_g + 1;
                            c_ph = c_ph + 1;
                            c_s = c_s + 1;
                        end
                    end                                                        
                    eval_rows_g(x)= assoc_g/c_g;
                    eval_rows_ph(x)= assoc_ph/c_ph;
                    eval_rows_s(x)= assoc_s/c_s;                    
                end                
            end
            tot_rows_ph{tclust,1}= clust_kept(i);  %%was i!!
            tot_rows_ph{tclust,2}= mean(eval_rows_ph);             
            tot_rows_g{tclust,1}= clust_kept(i);
            tot_rows_g{tclust,2}= mean(eval_rows_g);     
            tot_rows_s{tclust,1}= clust_kept(i);
            tot_rows_s{tclust,2}= mean(eval_rows_s);                                                                              
        end
                                  
    end
    
    clear ss 
    
end

t=table(clust_kept',clust_new',cell2mat(tot_rows_g(:,2)),cell2mat(tot_rows_ph(:,2)),...
    cell2mat(tot_rows_s(:,2)),'VariableNames',{'clust_old','clust_new','gen','phen','sem'})
writetable(t,evalfile_rows_dir,'delimiter','\t');




