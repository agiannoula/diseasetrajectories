#Disease trajectory clustering software
#Copyright (C) 2020 Alexia Giannoula and Laura I. Furlong, IBI group.
#This file is part of the Disease trajectory and clustering software

#The Disease trajectory and clustering software is free software: you can redistribute it and/or modify it under 
#the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 
#of the License, or (at your option) any later version (https://www.gnu.org/licenses/gpl-3.0.html).
 
#Disease trajectory and clustering software is distributed in the hope that it will be useful,but WITHOUT 
#ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
#See the GNU General Public License for more details.
 
#You should have received a copy of the GNU General Public License
#along with this program.  If not, see <http://www.gnu.org/licenses/>.
 
#How to cite Disease trajectory and clustering software:
#Giannoula, A., Centeno, E., Mayer M.-A., Sanz, F., & Furlong, L. I. A system-level analysis of patient disease 
#trajectories based on clinical, phenotypic and molecular similarities. Submitted to Bioinformatics.

import sys
from datetime import datetime

PAT_ID_1_COL = 0
PAT_ID_2_COL = 1
SIZE_COL = 2
DISEASE_1_1_COL = 3
DISEASE_1_2_COL = 4
DISEASE_1_3_COL = 5
DISEASE_1_4_COL = 6
DISEASE_1_5_COL = 7
DISEASE_1_6_COL = 8
DISEASE_2_1_COL = 9
DISEASE_2_2_COL = 10
DISEASE_2_3_COL = 11
DISEASE_2_4_COL = 12
DISEASE_2_5_COL = 13
DISEASE_2_6_COL = 14
DATE_1_1_COL = 15
DATE_1_2_COL = 16
DATE_1_3_COL = 17
DATE_1_4_COL = 18
DATE_1_5_COL = 19
DATE_1_6_COL = 20
DATE_2_1_COL = 21
DATE_2_2_COL = 22
DATE_2_3_COL = 23
DATE_2_4_COL = 24
DATE_2_5_COL = 25
DATE_2_6_COL = 26
AGE_1_1_COL = 27
AGE_1_2_COL = 28
AGE_1_3_COL = 29
AGE_1_4_COL = 30
AGE_1_5_COL = 31
AGE_1_6_COL = 32
AGE_2_1_COL = 33
AGE_2_2_COL = 34
AGE_2_3_COL = 35
AGE_2_4_COL = 36
AGE_2_5_COL = 37
AGE_2_6_COL = 38

FLAG = 39
NUMBER_ALERT = 1000000

def add_one_dictionary(dictionary, key):
    aux = dictionary.get(key, 0)
    dictionary[key] = aux + 1
    return dictionary

def disease_pairs_conunter(filename_path, output_path):
    
    
    ofile = open(output_path, "w")
    
    pair_dict = {}
    j=0
    i=1
    print "STEP 1: READING FILE"
    for lin in file(filename_path):
        j+=1
        if i == NUMBER_ALERT:
            print j
            i=0
        i+=1
        
        
        fields = lin.strip().split("\t")
        pat1 = fields[PAT_ID_1_COL]
        pat2 = fields[PAT_ID_2_COL]
	dis11 = fields[DISEASE_1_1_COL]
        dis12 = fields[DISEASE_1_2_COL]
	dis13 = fields[DISEASE_1_3_COL]
	dis14 = fields[DISEASE_1_4_COL]
	dis15 = fields[DISEASE_1_5_COL]
	dis16 = fields[DISEASE_1_6_COL]
        dis21 = fields[DISEASE_2_1_COL]
        dis22 = fields[DISEASE_2_2_COL]
	dis23 = fields[DISEASE_2_3_COL]
	dis24 = fields[DISEASE_2_4_COL]
	dis25 = fields[DISEASE_2_5_COL]
	dis26 = fields[DISEASE_2_6_COL]
        date11 =  fields[DATE_1_1_COL]
        date12 =  fields[DATE_1_2_COL]
        date13 =  fields[DATE_1_3_COL]
        date14 =  fields[DATE_1_4_COL]
        date15 =  fields[DATE_1_5_COL]
        date16 =  fields[DATE_1_6_COL]
        date21 =  fields[DATE_2_1_COL]
        date22 =  fields[DATE_2_2_COL]
        date23 =  fields[DATE_2_3_COL]
        date24 =  fields[DATE_2_4_COL]
        date25 =  fields[DATE_2_5_COL]
        date26 =  fields[DATE_2_6_COL]
	age11 =  fields[AGE_1_1_COL]
        age12 =  fields[AGE_1_2_COL]
        age13 =  fields[AGE_1_3_COL]
        age14 =  fields[AGE_1_4_COL]
        age15 =  fields[AGE_1_5_COL]
        age16 =  fields[AGE_1_6_COL]
        age21 =  fields[AGE_2_1_COL]
        age22 =  fields[AGE_2_2_COL]
        age23 =  fields[AGE_2_3_COL]
        age24 =  fields[AGE_2_4_COL]
        age25 =  fields[AGE_2_5_COL]
        age26 =  fields[AGE_2_6_COL]
	
        pair_pat1 = "\t".join([dis11, dis12, dis13, dis14, dis15, dis16, pat1])
        pair_pat2 = "\t".join([dis21, dis22, dis23, dis24, dis25, dis26, pat2])
       
	
        if not pair_pat1 in pair_dict:
            period1 = datetime.strptime(date12, '%Y-%m-%d') - datetime.strptime(date11, '%Y-%m-%d')
	    period2 = datetime.strptime(date13, '%Y-%m-%d') - datetime.strptime(date11, '%Y-%m-%d')
	    period3 = datetime.strptime(date14, '%Y-%m-%d') - datetime.strptime(date11, '%Y-%m-%d')
	    period4 = datetime.strptime(date15, '%Y-%m-%d') - datetime.strptime(date11, '%Y-%m-%d')
	    period5 = datetime.strptime(date16, '%Y-%m-%d') - datetime.strptime(date11, '%Y-%m-%d')
                                                
            pair_dict[pair_pat1] = [period1.days, period2.days, period3.days, period4.days, period5.days, int(age11), int(age12), int(age13), int(age14), int(age15), int(age16)]	    
        
        if not pair_pat2 in pair_dict:
            period1 = datetime.strptime(date22, '%Y-%m-%d') - datetime.strptime(date21, '%Y-%m-%d')
            period2 = datetime.strptime(date23, '%Y-%m-%d') - datetime.strptime(date21, '%Y-%m-%d')
	    period3 = datetime.strptime(date24, '%Y-%m-%d') - datetime.strptime(date21, '%Y-%m-%d')
	    period4 = datetime.strptime(date25, '%Y-%m-%d') - datetime.strptime(date21, '%Y-%m-%d')
	    period5 = datetime.strptime(date26, '%Y-%m-%d') - datetime.strptime(date21, '%Y-%m-%d')

            pair_dict[pair_pat2] = [period1.days, period2.days, period3.days, period4.days, period5.days, int(age21), int(age22), int(age23), int(age24), int(age25), int(age26)]	    
	
  
    pair_final_dict = {}
    pair_periods1_dict = {}
    pair_periods2_dict = {}
    pair_periods3_dict = {}
    pair_periods4_dict = {}
    pair_periods5_dict = {}
    pair_ages1_dict = {}
    pair_ages2_dict = {}
    pair_ages3_dict = {}
    pair_ages4_dict = {}
    pair_ages5_dict = {}
    pair_ages6_dict = {}
      

    print "STEP 2: PAIRS MANAGEMENT A"
    count = len(pair_dict)
    divi = 1000
    
    nalert = count/divi
    j=0
    i =1
    for pair in pair_dict:
        j+=1
        if i == nalert:
            i=0
            print j, "/", count
        i+=1
        
        fields = pair.split("\t")
	period1 = pair_dict[pair][0]
	period2 = pair_dict[pair][1]
	period3 = pair_dict[pair][2]
	period4 = pair_dict[pair][3]
	period5 = pair_dict[pair][4]
        age1 = pair_dict[pair][5]
        age2 = pair_dict[pair][6]	
	age3 = pair_dict[pair][7]
	age4 = pair_dict[pair][8]
	age5 = pair_dict[pair][9]
	age6 = pair_dict[pair][10]
	        
	pair = "\t".join([fields[0], fields[1], fields[2], fields[3], fields[4], fields[5]])
        add_one_dictionary(pair_final_dict,pair)

        aux_p1 = pair_periods1_dict.get(pair,0)
        pair_periods1_dict[pair] = aux_p1 + period1

        aux_p2 = pair_periods2_dict.get(pair,0)
        pair_periods2_dict[pair] = aux_p2 + period2

        aux_p3 = pair_periods3_dict.get(pair,0)
        pair_periods3_dict[pair] = aux_p3 + period3

        aux_p4 = pair_periods4_dict.get(pair,0)
        pair_periods4_dict[pair] = aux_p4 + period4

        aux_p5 = pair_periods5_dict.get(pair,0)
        pair_periods5_dict[pair] = aux_p5 + period5

        aux1 = pair_ages1_dict.get(pair,0)
        pair_ages1_dict[pair] = aux1 + age1

        aux2 = pair_ages2_dict.get(pair,0)
        pair_ages2_dict[pair] = aux2 + age2

        aux3 = pair_ages3_dict.get(pair,0)
        pair_ages3_dict[pair] = aux3 + age3

        aux4 = pair_ages4_dict.get(pair,0)
        pair_ages4_dict[pair] = aux4 + age4

        aux5 = pair_ages5_dict.get(pair,0)
        pair_ages5_dict[pair] = aux5 + age5

        aux6 = pair_ages6_dict.get(pair,0)
        pair_ages6_dict[pair] = aux6 + age6

       
    print "STEP 3: FINAL"

    count = len(pair_dict)
    divi = 1000
    nalert = count/divi
    j=0
    i =1
    for pair in pair_final_dict:
        j+=1
        if i == nalert:
            i=0
            print j, "/", count
        i+=1
        value = pair_final_dict[pair]
        per1 = float(pair_periods1_dict[pair])/value
	per2 = float(pair_periods2_dict[pair])/value
	per3 = float(pair_periods3_dict[pair])/value
	per4 = float(pair_periods4_dict[pair])/value
	per5 = float(pair_periods5_dict[pair])/value
	a1 = float(pair_ages1_dict[pair])/value
	a2 = float(pair_ages2_dict[pair])/value
	a3 = float(pair_ages3_dict[pair])/value
	a4 = float(pair_ages4_dict[pair])/value
	a5 = float(pair_ages5_dict[pair])/value
	a6 = float(pair_ages6_dict[pair])/value

        ofile.write(pair +"\t"+ str(value) + "\t" + str(per1) + "\t" + str(per2) + "\t" + str(per3) + "\t" + str(per4) + "\t" + str(per5) + "\t" + str(a1) + "\t" + str(a2) + "\t" + str(a3) + "\t" + str(a4) + "\t" + str(a5) + "\t" + str(a6) + "\n")
        ofile.flush()
    
    ofile.close()
      
    
if __name__ == '__main__':
    

    filename_path = sys.argv[1]

    output_path = "diseases6.txt"	


    disease_pairs_conunter(filename_path, output_path)
