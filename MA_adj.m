%Disease trajectory and clustering software is distributed in the hope that it will be useful,but WITHOUT 
%ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
%See the GNU General Public License for more details.
 
%You should have received a copy of the GNU General Public License
%along with this program.  If not, see <http://www.gnu.org/licenses/>.
 
%How to cite Disease trajectory and clustering software:
%Giannoula, A., Centeno, E., Mayer M.-A., Sanz, F., & Furlong, L. I. A system-level analysis of patient disease 
%trajectories based on clinical, phenotypic and molecular similarities. Submitted to Bioinformatics.


clear all

B = '185';  %disease of interest: prostate cancer

%age group1
%format: dis1 dis2 #pat Dt age1 age2 p-value RR n_d1d2 n_d1 n_d2 n_notd1d2 Dir
fstats1 = readtable('fisher_R1.txt','delimiter',' ','format','%s %s %d %f %f %f %f %f %d %d %d %d');
row = fstats1.fisher < 0.05;
tf1 = fstats1(row,:);
size(tf1)

%age group2
%format: dis1 dis2 #pat Dt age1 age2 p-value RR n_d1d2 n_d1 n_d2 n_notd1d2 Dir
fstats2 = readtable('fisher_R2.txt','delimiter',' ','format','%s %s %d %f %f %f %f %f %d %d %d %d');
row = fstats2.fisher < 0.05;
tf2 = fstats2(row,:);
size(tf2)


u=union(fstats1(:,1:2),fstats2(:,1:2),'rows');


%shorter table for RR, RR1, RR2
for i=1:size(u,1)
    row = ismember(fstats1(:,1:2), u(i,:));
    t_old =fstats1(row,:);
    row = ismember(fstats2(:,1:2), u(i,:));
    t_elder =fstats2(row,:); 
    pv1(i) = -10; pv2(i) = -10;
    if (~isempty(t_old))
        pv1(i) = t_old.fisher;
    end
    if (~isempty(t_elder))
        pv2(i) = t_elder.fisher;
    end
    rr_c1(i) = -20; rr_c2(i) = -20;
    if (~isempty(t_old))
        rr_c1(i) = t_old.relativeRisk;
    end
    if (~isempty(t_elder))
        rr_c2(i) = t_elder.relativeRisk;
    end
    
    
    if (~isempty(t_old) & ~isempty(t_elder))        
        X(:,:,1) = [t_old.AB t_old.AnotB; t_old.BnotA t_old.notAnotB];
        X(:,:,2) = [t_elder.AB t_elder.AnotB; t_elder.BnotA t_elder.notAnotB];
        [RR_adj, RR1, RR2, pv] = MantelHaenTest_RR(X);           
        pat_tot = t_old.pat + t_elder.pat;        
        age1 = double(t_old.age1 + t_elder.age1)/2;
        age2 = double(t_old.age2 + t_elder.age2)/2;
        dt = (t_old.dt + t_elder.dt)/2;
        adj_flag = 'Y';        
    elseif (~isempty(t_old) & isempty(t_elder))             
        RR_adj = t_old.relativeRisk;    
        pat_tot = t_old.pat;  
        cij = pat_tot;        
        Ni = double(t_old.AB + t_old.AnotB);
        Nj = double(t_old.notAnotB + t_old.BnotA);
        Pi = double(t_old.disA)/Ni; Pj = double(t_old.disB)/Nj;
        lim1 = log(RR_adj)-1.96*sqrt((1-Pi)/Pi/Ni+(1-Pj)/Pj/Nj);
        lim2 = log(RR_adj)+1.96*sqrt((1-Pi)/Pi/Ni+(1-Pj)/Pj/Nj);        
        RR1 = exp(lim1);
        RR2 = exp(lim2);   
        age1 = double(t_old.age1);
        age2 = double(t_old.age2);
        dt = t_old.dt;
        adj_flag = 'N';
    elseif (isempty(t_old) & ~isempty(t_elder))        
        RR_adj = t_elder.relativeRisk;
        pat_tot = t_elder.pat;   
        cij = pat_tot;        
        Ni = double(t_elder.AB + t_elder.AnotB);
        Nj = double(t_elder.notAnotB + t_elder.BnotA);
        Pi = double(t_elder.disA)/Ni; Pj = double(t_elder.disB)/Nj;
        lim1 = log(RR_adj)-1.96*sqrt((1-Pi)/Pi/Ni+(1-Pj)/Pj/Nj);
        lim2 = log(RR_adj)+1.96*sqrt((1-Pi)/Pi/Ni+(1-Pj)/Pj/Nj);        
        RR1 = exp(lim1);
        RR2 = exp(lim2);
        age1 = double(t_elder.age1);
        age2 = double(t_elder.age2);
        dt = t_elder.dt;
        adj_flag = 'N';        
    else
        input('error_case...');          
    end
    
    RR(i) = RR_adj;    
    pat(i) = pat_tot;
    age_1(i) = age1;
    age_2(i) = age2;
    deltat(i) =dt;
    adj{i} = adj_flag;
end
     
t.dis1 = u.disAcode;
t.dis2 = u.disBcode;
t.pat = pat';
t.RR = RR';
t.RR1 = RR_1f';
t.RR2 = RR_2f';
t.age1 = age_1';
t.age2 = age_2';
t.dt = deltat';
t.adj = adj';

pairs_fin = struct2table(t);

pairs_fin = sortrows(pairs_fin,4);
rows = pairs_fin.pv < 0.05; %only statistically significant adjusted relative risk
pairs = pairs_fin(rows,:)

%save pairs with RRadj
writetable(pairs,'pairs_RRadj.txt','delimiter','\t');
   
