%Disease trajectory clustering software
%Copyright (C) 2020 Alexia Giannoula and Laura I. Furlong, IBI group.
%This file is part of the Disease trajectory and clustering software

%The Disease trajectory and clustering software is free software: you can redistribute it and/or modify it under 
%the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 
%of the License, or (at your option) any later version (https://www.gnu.org/licenses/gpl-3.0.html).
 
%Disease trajectory and clustering software is distributed in the hope that it will be useful,but WITHOUT 
%ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
%See the GNU General Public License for more details.
 
%You should have received a copy of the GNU General Public License
%along with this program.  If not, see <http://www.gnu.org/licenses/>.
 
%How to cite Disease trajectory and clustering software:
%Giannoula, A., Centeno, E., Mayer M.-A., Sanz, F., & Furlong, L. I. A system-level analysis of patient disease 
%trajectories based on clinical, phenotypic and molecular similarities. Submitted to Bioinformatics.

%%%% put together all trajectories that enter in clustering algorithm
 
clear all


N = 2; 
thres2 = input('Threshold for #patients for trajectories of 2:' );
%read disease trajectory file length 2: dis1 dis2 #pat Dt age1 age2 p-value n_d1d2 n_d1 n_d2 n_notd1d2 Dir
%where Dt is the averaged time difference in days between the two
%diagnostics, age1, age2 denote the average age of the patients at the
%time of each diagnosis, p-value results from the Fisher's exact test where 
%n_d1d2, n_d1, n_d2, n_notd1d2 are the number of patients of the cohort with both diseases,
%with dis1, with dis2 and without any of dis1, dis2, respectively. Dir denotes the preferred directionality.
t2 = readtable('diseases2final.txt','delimiter','\t','format','%s %s %d %f %f %f %f %d %d %d %d %d'); 
row = t2.pat >= thres2;
t2 = t2(row,:);


N = 3; 
thres3 = input('Threshold for #patients for trajectories of 3:' );
%read trajectory file length 3: dis1 dis2 dis3 #pat Dt1 Dt2 age1 age2 age3
%where Dt1, Dt2 are the averaged time differences in days between the
%different diagnostics and age1, age2, age3 denote the average age of the patients at the
%time of each diagnosis. 
t3 = readtable('diseases3.txt','delimiter','\t','format','%s %s %s %d %f %f %f %f %f');
row = t3.pat >= thres3;  
t3 = t3(row,:);


N = 4;
thres4 = input('Threshold for #patients for trajectories of 4:' );
%read disease trajectory file length 4, similarly as above 
%dis1 dis2 dis3 dis4 #pat Dt1 Dt2 Dt3 age1 age2 age3 age4
t4 = readtable('diseases4.txt','delimiter','\t','format','%s %s %s %s %d %f %f %f %f %f %f %f');
row = t4.pat >= thres4;  
t4 = t4(row,:);


N = 5;
thres5 = input('Threshold for #patients for trajectories of 5:' );
%read disease trajectory file length 5, similarly as above 
%dis1 dis2 dis3 dis4 dis5 #pat Dt1 Dt2 Dt3 Dt4 age1 age2 age3 age4 age5
t5 = readtable('diseases5.txt','delimiter','\t','format','%s %s %s %s %s %d %f %f %f %f %f %f %f %f %f');
row = t5.pat >= thres5;  
t5 = t5(row,:);

N = 6;
thres6 = input('Threshold for #patients for trajectories of 6:' );
%read disease trajectory file length 6, similarly as above 
%dis1 dis2 dis3 dis4 dis5 dis6 #pat Dt1 Dt2 Dt3 Dt4 Dt5 age1 age2 age3 age4
%age5
t6 = readtable('diseases6.txt','delimiter','\t','format','%s %s %s %s %s %s %d %f %f %f %f %f %f %f %f %f %f %f');
row = t6.pat >= thres6;  
t6 = t6(row,:);


disp('TRAJECTORIES COLLECTED:')
disp('traj len-2:')
size(t2,1)
disp('traj len-3:')
size(t3,1)
disp('traj len-4:')
size(t4,1)
disp('traj len-5:')
size(t5,1)
disp('traj len-6:')
size(t6,1)

save t2 t2 
save t3 t3
save t4 t4
save t5 t5
save t6 t6

