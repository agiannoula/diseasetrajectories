%Disease trajectory clustering software
%Copyright (C) 2020 Alexia Giannoula and Laura I. Furlong, IBI group.
%This file is part of the Disease trajectory and clustering software

%The Disease trajectory and clustering software is free software: you can redistribute it and/or modify it under 
%the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 
%of the License, or (at your option) any later version (https://www.gnu.org/licenses/gpl-3.0.html).
 
%Disease trajectory and clustering software is distributed in the hope that it will be useful,but WITHOUT 
%ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
%See the GNU General Public License for more details.
 
%You should have received a copy of the GNU General Public License
%along with this program.  If not, see <http://www.gnu.org/licenses/>.
 
%How to cite Disease trajectory and clustering software:
%Giannoula, A., Centeno, E., Mayer M.-A., Sanz, F., & Furlong, L. I. A system-level analysis of patient disease 
%trajectories based on clinical, phenotypic and molecular similarities. Submitted to Bioinformatics.

%%%% create a map with all semantic distances (Sokal) between all pairwise
%%%% ICD-9 codes


clear all

N = 2;
load t2
t = t2;
u2 = unique(table2array(t(:,1:N)));


N = 3;
load t3 
t = t3;
u3 = unique(table2array(t(:,1:N)));

N = 4;
load t4
t = t4;
u4 = unique(table2array(t(:,1:N)));

N = 5;
load t5
t = t5;
u5 = unique(table2array(t(:,1:N)));

N = 6;
load t6
t = t6;
u6 = unique(table2array(t(:,1:N)));

u =[u2; u3; u4; u5; u6];
u = cellstr(u);
u = unique(u);

L = size(u,1)


%setup DATABASE connection with MYSQL
%semantic similarity database
instance = input('Instance for database: ');
username = input('Username for database: ');
password = input('Password for database: ');

conn = database(instance, username, password);


qu1 = "SELECT icd9cm_allsab_diso.cui FROM semantic_similarity.icd9cm_allsab_diso where icd9cm_allsab_diso.code regexp '(^| )";
qu3 = "($| )'";

qu_fin_a = "SELECT ytex_similarity_icd9cm_allsab_diso.sokal from semantic_similarity.ytex_similarity_icd9cm_allsab_diso where ytex_similarity_icd9cm_allsab_diso.cui1 = '";
qu_fin_b = "' AND ytex_similarity_icd9cm_allsab_diso.cui2 = '";
qu_fin_c = char(39);

keyset = strings(L,L); valueset = zeros(L,L); flag = zeros(L,L);
q=1;
for i=1:L
  
    for j=1:L
        
        if (flag(i,j) == 0)
        
            r = u{i};
            dot_r = strfind(r,'.');
            if (~isempty(dot_r))            
                Lr = length(r);
                r_beg = r(1:dot_r-1);
                r_end = r(dot_r+1:Lr);
                if (dot_r == 2)
                    r_beg = strcat('00',r_beg);
                elseif (dot_r == 3)
                    r_beg = strcat('0',r_beg);
                end       
                rr = strcat(strcat(r_beg,'\\.'),r_end);
            else
                if (length(r) == 1)
                    rr = strcat('00',r);
                elseif (length(r) == 2)
                    rr = strcat('0',r);
                else
                    rr = r;
                end            
            end        

            t = u{j};  

            dot_t = strfind(t,'.');
            if (~isempty(dot_t))            
                Lt = length(t);                            
                t_beg = t(1:dot_t-1);
                t_end = t(dot_t+1:Lt);
                if (dot_t == 2)
                    t_beg = strcat('00',t_beg);
                elseif (dot_t == 3)
                    t_beg = strcat('0',t_beg);
                end            
                tt = strcat(strcat(t_beg,'\\.'),t_end);
            else
                if (length(t) == 1)
                    tt = strcat('00',t);
                elseif (length(t) == 2)
                    tt = strcat('0',t);
                else
                    tt = t;
                end
            end
                
            query_r=strcat(strcat(qu1,rr),qu3);               
            res_r=select(conn,query_r);
            query_t=strcat(strcat(qu1,tt),qu3);                
            res_t=select(conn,query_t);  
            strkey1 = strcat(strcat(r,'_'),t);
            strkey2 = strcat(strcat(t,'_'),r);
            if (isempty(res_r) ~= 1 & isempty(res_t) ~= 1)

                query_fin = strcat(strcat(strcat(qu_fin_a,res_r.cui),strcat(qu_fin_b,res_t.cui)),qu_fin_c);        
                res = select(conn,query_fin);            
                dist =res.sokal;
            elseif (isempty(res_r) | isempty(res_t))   
                input('empty...')
                dist = 0;        
            end    
            
            keyset(i,j) = strkey1;       
            valueset(i,j) = dist;
            flag(i,j) = 1;
            keyset(j,i) = strkey2;
            valueset(j,i) = dist;
            flag(j,i) = 1;   
        end
    end    
        
end


map_sem_sok = containers.Map(cellstr(keyset),valueset);

sem_flag = flag;



save map_sem_sok map_sem_sok
save sem_flag sem_flag

close(conn);
