#Disease trajectory clustering software
#Copyright (C) 2020 Alexia Giannoula and Laura I. Furlong, IBI group.
#This file is part of the Disease trajectory and clustering software

#The Disease trajectory and clustering software is free software: you can redistribute it and/or modify it under 
#the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 
#of the License, or (at your option) any later version (https://www.gnu.org/licenses/gpl-3.0.html).
 
#Disease trajectory and clustering software is distributed in the hope that it will be useful,but WITHOUT 
#ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
#See the GNU General Public License for more details.
 
#You should have received a copy of the GNU General Public License
#along with this program.  If not, see <http://www.gnu.org/licenses/>.
 
#How to cite Disease trajectory and clustering software:
#Giannoula, A., Centeno, E., Mayer M.-A., Sanz, F., & Furlong, L. I. A system-level analysis of patient disease 
#trajectories based on clinical, phenotypic and molecular similarities. Submitted to Bioinformatics.

import sys
from datetime import datetime


PAT_ID_1_COL = 0
PAT_ID_2_COL = 1

DISEASE_1_1_COL = 3
DISEASE_1_2_COL = 4
DISEASE_1_3_COL = 5
DISEASE_2_1_COL = 6
DISEASE_2_2_COL = 7
DISEASE_2_3_COL = 8
DATE_1_1_COL = 9
DATE_1_2_COL = 10
DATE_1_3_COL = 11
DATE_2_1_COL = 12
DATE_2_2_COL = 13
DATE_2_3_COL = 14
AGE_1_1_COL = 15
AGE_1_2_COL = 16
AGE_1_3_COL = 17
AGE_2_1_COL = 18
AGE_2_2_COL = 19
AGE_2_3_COL = 20

FLAG = 21
NUMBER_ALERT = 1000000

def add_one_dictionary(dictionary, key):
    aux = dictionary.get(key, 0)
    dictionary[key] = aux + 1
    return dictionary

def disease_pairs_conunter(filename_path, output_path):
    
    
    ofile = open(output_path, "w")
    
    pair_dict = {}
    j=0
    i=1
    print "STEP 1: READING FILE"
    for lin in file(filename_path):
        j+=1
        if i == NUMBER_ALERT:
            print j
            i=0
        i+=1
        
        
        fields = lin.strip().split("\t")
        pat1 = fields[PAT_ID_1_COL]
        pat2 = fields[PAT_ID_2_COL]
        dis11 = fields[DISEASE_1_1_COL]
        dis12 = fields[DISEASE_1_2_COL]
	dis13 = fields[DISEASE_1_3_COL]
        dis21 = fields[DISEASE_2_1_COL]
        dis22 =  fields[DISEASE_2_2_COL]
	dis23 = fields[DISEASE_2_3_COL]
        date11 =  fields[DATE_1_1_COL]
        date12 =  fields[DATE_1_2_COL]
        date13 =  fields[DATE_1_3_COL]
        date21 =  fields[DATE_2_1_COL]
        date22 =  fields[DATE_2_2_COL]
        date23 =  fields[DATE_2_3_COL]
	age11 =  fields[AGE_1_1_COL]
        age12 =  fields[AGE_1_2_COL]
        age13 =  fields[AGE_1_3_COL]
        age21 =  fields[AGE_2_1_COL]
        age22 =  fields[AGE_2_2_COL]
        age23 =  fields[AGE_2_3_COL]
	
        
        pair_pat1 = "\t".join([dis11, dis12, dis13, pat1])
        pair_pat2 = "\t".join([dis21, dis22, dis23, pat2])
	
	
        if not pair_pat1 in pair_dict:
            period1 = datetime.strptime(date12, '%Y-%m-%d') - datetime.strptime(date11, '%Y-%m-%d')
	    period2 = datetime.strptime(date13, '%Y-%m-%d') - datetime.strptime(date11, '%Y-%m-%d')                        
            pair_dict[pair_pat1] = [period1.days, period2.days, int(age11), int(age12), int(age13)]	    
        
        if not pair_pat2 in pair_dict:
            period1 = datetime.strptime(date22, '%Y-%m-%d') - datetime.strptime(date21, '%Y-%m-%d')
            period2 = datetime.strptime(date23, '%Y-%m-%d') - datetime.strptime(date21, '%Y-%m-%d')
            pair_dict[pair_pat2] = [period1.days, period2.days, int(age21), int(age22), int(age23)]	    
	
  
    pair_final_dict = {}
    pair_periods1_dict = {}
    pair_periods2_dict = {}
    pair_ages1_dict = {}
    pair_ages2_dict = {}
    pair_ages3_dict = {}
       
    print "STEP 2: PAIRS MANAGEMENT A"
    count = len(pair_dict)
    divi = 1000
    
    nalert = count/divi
    j=0
    i =1
    for pair in pair_dict:
        j+=1
        if i == nalert:
            i=0
            print j, "/", count
        i+=1
        
        fields = pair.split("\t")
	period1 = pair_dict[pair][0]
	period2 = pair_dict[pair][1]
        age1 = pair_dict[pair][2]
        age2 = pair_dict[pair][3]	
	age3 = pair_dict[pair][4]

        pair = "\t".join([fields[0], fields[1],fields[2]])
        add_one_dictionary(pair_final_dict,pair)

        aux_p1 = pair_periods1_dict.get(pair,0)
        pair_periods1_dict[pair] = aux_p1 + period1

        aux_p2 = pair_periods2_dict.get(pair,0)
        pair_periods2_dict[pair] = aux_p2 + period2

        aux1 = pair_ages1_dict.get(pair,0)
        pair_ages1_dict[pair] = aux1 + age1

        aux2 = pair_ages2_dict.get(pair,0)
        pair_ages2_dict[pair] = aux2 + age2

        aux3 = pair_ages3_dict.get(pair,0)
        pair_ages3_dict[pair] = aux3 + age3

       
    print "STEP 3: FINAL"

    count = len(pair_dict)
    divi = 1000
    nalert = count/divi
    j=0
    i =1
    for pair in pair_final_dict:
        j+=1
        if i == nalert:
            i=0
            print j, "/", count
        i+=1
        value = pair_final_dict[pair]
        per1 = float(pair_periods1_dict[pair])/value
	per2 = float(pair_periods2_dict[pair])/value
	a1 = float(pair_ages1_dict[pair])/value
	a2 = float(pair_ages2_dict[pair])/value
	a3 = float(pair_ages3_dict[pair])/value

        ofile.write(pair +"\t"+ str(value) + "\t" + str(per1) + "\t" + str(per2) + "\t" + str(a1) + "\t" + str(a2) + "\t" + str(a3) + "\n")
        ofile.flush()
    
    ofile.close()
      
    
if __name__ == '__main__':
    

    filename_path = sys.argv[1]

    
    output_path = "diseases3.txt"	

    disease_pairs_conunter(filename_path, output_path)
