%Disease trajectory clustering software
%Copyright (C) 2020 Alexia Giannoula and Laura I. Furlong, IBI group.
%This file is part of the Disease trajectory and clustering software

%The Disease trajectory and clustering software is free software: you can redistribute it and/or modify it under 
%the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 
%of the License, or (at your option) any later version (https://www.gnu.org/licenses/gpl-3.0.html).
 
%Disease trajectory and clustering software is distributed in the hope that it will be useful,but WITHOUT 
%ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
%See the GNU General Public License for more details.
 
%You should have received a copy of the GNU General Public License
%along with this program.  If not, see <http://www.gnu.org/licenses/>.
 
%How to cite Disease trajectory and clustering software:
%Giannoula, A., Centeno, E., Mayer M.-A., Sanz, F., & Furlong, L. I. A system-level analysis of patient disease 
%trajectories based on clinical, phenotypic and molecular similarities. Submitted to Bioinformatics.

%%%% map ICD9-CUI, CUI-HPO

clear all

N = 2;
load t2
t = t2;
u2 = unique(table2array(t(:,1:N)));


N = 3;
load t3 
t = t3;
u3 = unique(table2array(t(:,1:N)));

N = 4;
load t4
t = t4;
u4 = unique(table2array(t(:,1:N)));

N = 5;
load t5
t = t5;
u5 = unique(table2array(t(:,1:N)));

N = 6;
load t6
t = t6;
u6 = unique(table2array(t(:,1:N)));

u =[u2; u3; u4; u5; u6];

u = cellstr(u);
u = unique(u);

L = size(u,1)

load map_phen_id
load map_phen_dist
load map_phen_iic_lch

keyset = strings(L,L); valueset = ones(L,L);   
flag = zeros(L,L); q = zeros(L,L);

penalflag = 1;

for i=1:L  
    r = u{i};        
    for j=1:L        
        
        t = u{j};  
                
        if (i~=j)
                         
            A = map_phen_id(r); 
            B = map_phen_id(t); 

            penal1 = 1; 
            penal2 = 1;
            if (penalflag == 1)
                w1 = map_phen_dist(r); 
                w2 = map_phen_dist(t);
                if (w1>0 & w1 <1)
                    penal1 = w1;        
                end
                if (w2>0 & w2 <1)
                    penal2 = w2;        
                end
            end        
            dist = penal1*penal2*BMA_phen(A, B, map_phen_iic_lch);                
            dist_no = BMA_phen(A, B, map_phen_iic_lch);                
            valueset(i,j) = dist;
            valueset(j,i) = dist; 
            strkey1 = strcat(strcat(r,'_'),t);
            strkey2 = strcat(strcat(t,'_'),r); 
            keyset(i,j) = strkey1;               
            flag(i,j) = 1;
            keyset(j,i) = strkey2;        
            flag(j,i) = 1;   
        else                                               
            strkey1 = strcat(strcat(r,'_'),t);        
            keyset(i,j) = strkey1;               
            flag(i,j) = 1;
        end                 
    end
   
    
end
    
map_dis_phen = containers.Map(cellstr(keyset),valueset);

save map_dis_phen map_dis_phen

