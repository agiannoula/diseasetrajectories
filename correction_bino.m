%Disease trajectory and clustering software is distributed in the hope that it will be useful,but WITHOUT 
%ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
%See the GNU General Public License for more details.
 
%You should have received a copy of the GNU General Public License
%along with this program.  If not, see <http://www.gnu.org/licenses/>.
 
%How to cite Disease trajectory and clustering software:
%Giannoula, A., Centeno, E., Mayer M.-A., Sanz, F., & Furlong, L. I. A system-level analysis of patient disease 
%trajectories based on clinical, phenotypic and molecular similarities. Submitted to Bioinformatics.

clear all

%%%%%%%% load fisher results %%%%%%%%%%%%%%%%%%%%
stats = readtable('diseases2fisher.txt','delimiter',' ','format','%s %s %d %f %f %f %f %d %d %d %d');	
   
row = stats.pat >= 10;
stats = stats(row,:);

row = stats.rr > 1;
stats = stats(row,:);


stats05 = stats;


[n,cl] = size(stats05)
Q = 0.01; %cutoff

stats05.rank = zeros(n,1);
r = 1;
i = 1;
while (i<=n)
    if (stats05.rank(i) ~= 0)
        i = i+1;
    else                
        
        b=ismember(stats05.dis1,stats05.dis2(i)) & ismember(stats05.dis2,stats05.dis1(i));
        ind = find(b==1);
        
        if (length(ind) == 1)
            stats05.rank(i) = r;
            stats05.rank(ind) = r;        
            r = r+1;            
        elseif isempty(ind)        
            stats05.rank(i) = r;        
            r = r+1;        
        elseif (length(ind) > 1)
            input('problem')
        end            
    
        i = i+1; 
    end
    if (mod(i,100)==0)
        disp(i);
    end        
end    

size(stats05)

m = length(unique(stats05.rank));

%bonferroni correction
pval = 0.001;
pnew = pval / m;

row = stats05.pv < pnew;
corr_bonf001 = stats05(row,:);

str = sprintf('the first %d trajectories are significant (BONF)',size(corr_bonf001,1));
disp('*****************************************')
disp(str)
disp('*****************************************')

writetable(corr_bonf001,'diseases2fisher_Bonf.txt','delimiter','\t');          

disp('file saved....');


%%%% include directionality 1,-1,0 in each association
t = corr_bonf001;
newt = 3*ones(size(t,1),1);
for i = 1: size(t,1)
    k = find(t.rank == t.rank(i));
    temp = t(k,:);
    
    if (size(temp,1) == 2)
    
    
        n1 = double(temp.pat(1));
        n2 = double(temp.pat(2));
        pv = binotest(n1,n1+n2,0.5,'two');
        if (pv < 0.05)
            if (n1 > n2)
                newt(k(1)) = 1;
                newt(k(2)) = -1;
            elseif (n1 < n2)
                newt(k(2)) = 1;
                newt(k(1)) = -1;                
            end
        else
            newt(k) = 0;
        end


    elseif (size(temp,1) == 1)
        newt(k) = 1;
    else
        input('different size....')
    end
          
end
t.dir = newt;
corr_bonf_dir = t;
writetable(corr_bonf_dir,'diseases2final.txt','delimiter','\t');   
clear t

