# Disease Trajectory Clustering software

*Disease trajectory clustering software*
Copyright (C) 2020 Alexia Giannoula and Laura I. Furlong, IBI group.
This file is part of the Disease trajectory and clustering software

 
The Disease trajectory and clustering software is free software: you can redistribute it and/or modify it under 
the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 
of the License, or (at your option) any later version (https://www.gnu.org/licenses/gpl-3.0.html).
 
Disease trajectory and clustering software is distributed in the hope that it will be useful,but WITHOUT 
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the GNU General Public License for more details.
 
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 
How to cite Disease trajectory and clustering software:
Giannoula, A., Centeno, E., Mayer M.-A., Sanz, F., & Furlong, L. I. A system-level analysis of patient disease 
trajectories based on clinical, phenotypic and molecular similarities. Submitted to Bioinformatics.

**Common disease trajectories**

**calc_traj_with_dates_age_id.py**

This script written in Python receives as input four data files from the health database (described below) 
and returns a list of files containing the common disease trajectories of various lengths, 
i.e. traj_2.txt, traj_3.txt, traj_4.txt, etc. The input files are the following:

*diseases.txt: This file contains rows with sequences of ICD9 disease codes (strings) for each patient, for example:

dis1_1 dis1_2 dis1_3   (describes the three diagnoses for patient 1)

dis2_1 dis2_2  (describes the two diagnoses for patient 2)

dis3_1 dis3_2 dis3_3 dis3_4  (describes the four diagnoses for patient 3)
….

*dates.txt: This file contains rows with sequences of the dates (strings) for each patient's diagnosis, for example.:

date1_1 date1_2 date1_3   (describes the dates of the three diagnoses for patient 1)

date2_1 date2_2 (describes the dates of the two diagnoses for patient 2)

date3_1 date3_2 date3_3 date3_4   (describes the dates of the four diagnoses for patient 3)
….

*ages.txt: This file contains rows with sequences of the age (integers) of each patient at the time of each diagnosis, for example.:

age1_1 age1_2 age1_3 (describes patient's 1 age at the date of each of the three diagnoses)

age2_1 age2_2   (describes patient's 2 age at the date of each of the two diagnoses)

age3_1 age3_2 age3_3 age3_4  (describes patient's 3 age at the date of each of the four diagnoses)
…

*info.txt: This file contains rows with additional info for each patient, i.e. patient id (integer), year of birth (integer), initial and final age (integers) of his/her clinical history, for example:

id1 year_birth1 initial_age1 final_age1  (contains patient's 1 id, year of birth, age at his first and last diagnosis)

id2 year_birth2 initial_age2 final_age2  (contains patient's 2 id, year of birth, age at his first and last diagnosis)

id3 year_birth3 initial_age3 final_age3 (contains patient's 3 id, year of birth, age at his first and last diagnosis)
….

**In order to identify a statistical association between two codes, the following processes should be performed:** 

The **fisher's exact test** to decide statistical significance (fisher_test.py), followed by **Bonferroni correction** and the **directionality analysis** based on the Binomial test (correction_bino.m) and the **MA test for age correction** (MA_adj.m). These processes are described in more detail below. 

**form_traj2.py**
This Python script takes the traj_2.txt output of the previous step and forms the final list of 
common disease trajectories of length 2, named diseases2.txt.

**fisher_test.py**
This script accepts as input the diseases2.txt file of the previous step,
together with the file containing the clinical histories of the control group of 
patients (without the disease) and performs the Fisher's exact test. 
It calls as follows: fisher_test.py diseases2.txt control.txt > diseases2fisher.txt

**correction_bino.m**
This script takes as input the previous file diseases2fisher.txt and performs a Bonferroni correction together with 
a directionality analysis and saves the results to a new text file named diseases2final.txt.

**MA_adj.m**
This script takes as input the Fisher's exact test files performed separately on two age groups,
adjusts for the confounding factor of age and saves the results to pairs_RRadj.txt. It calls the Mantel-Haenszel's 
funcion below.

**MantelHaenTest_RR.m**
This function performs an age adjustment of the RR according to the Mantel-Haenszel's test statistic.
It was developed by Steinar Thorvaldsen, steinar.thorvaldsen@uit.no, Dec. 2005. 
*Thorvaldsen, S. , Fl, T. and Willassen, N.P. (2010) DeltaProt: a software toolbox for comparative genomics. 
BMC Bioinformatics 2010, Vol 11:573.
See http://www.biomedcentral.com/1471-2105/11/573*

**form_traj3.py**
This Python script takes the traj_3.txt output of the previous steps and forms the final list of 
common disease trajectories of length 3, named diseases3.txt that will be used by collect_traj.m.

**form_traj4.py**
This Python script takes the traj_4.txt output of the previous steps and forms the final list of 
common disease trajectories of length 4, named diseases4.txt that will be used by collect_traj.m.

**form_traj5.py**
This Python script takes the traj_5.txt output of the previous steps and forms the final list of 
common disease trajectories of length 5, named diseases5.txt that will be used by collect_traj.m.

**form_traj6.py**
This Python script takes the traj_6.txt output of the previous steps and forms the final list of 
common disease trajectories of length 6, named diseases6.txt that will be used by collect_traj.m.

**collect_traj.m:**
In this file all common disease trajectories of the previous steps (diseases2final.txt, diseases3.txt, etc) 
are read as table files and exported as matlab variables (t2.mat, t3.mat, etc). Thresholds are also needed 
as input by the user to filter trajectories according to the number of patients sharing them.

**Clinical (semantic) clustering**

**createmap_sok.m:**
This script creates a map with all semantic similarity distances (Sokal metric) between all pairwise
ICD-9 codes involved in the study. First, the mat variables t2, t3, etc. of the previous script 
are loaded (representing the common disease trajectories of various lengths) and the list of all ICD-9 
codes is extracted. The Sokal distance between two codes is calculated with consecutive queries 
to the corresponding SQL database. The map is saved as *map_sem_sok* to be used as input in the following script.


**cluster_sem_map.m:**
The mat variables holding the common disease trajectories are loaded (t2, t3,...) in order to be clustered.
The map of semantic similarity distances formed in the previous step (*map_sem_sok*) is also needed as input 
into the clustering algorith, which is executed by calling the *dtw_SEM_map()* function, described below. Finally,
the clusters are saved in both a text file (*Q_str_thres_SEM.txt*) and matlab variables (*Qmat*, *CIDmat*). A file 
with stats info of the retrieved clusters is also saved separately (*Q_str_thres_SEM_info_srt.txt*) and *str_thres* denotes
the threshold that is implicated in the clustering algorithm to define the clustering granularity.


**dtw_SEM_map().m:**
It aligns two disease trajectories based on the Dynamic Time Warping (DTW) method. The *map_sem_sok* is needed
as input in order to form the local distance matrix with the pairwise semantic (clinical) similarities.


**Genetic clustering**

**createmap_gen.m:**
This script creates a map with the annotated sets of genes for all ICD-9 codes involved in the study. 
First, the mat variables t2, t3, etc. of the previous script are loaded (representing the common 
disease trajectories of various lengths) and the list of all ICD-9 codes is extracted. By quering the DisGeNET 
database (www.disgenet.org), each ICD-9 code is mapped to the corresponding UMLS concept unique identifier (CUI).
If a CUI is not associated to any gene, then a heuristic algorithm is executed, where the most semantically similar
CUI with at least one annotated gene is sought. This will substitute the CUI with missing gene annotation information
and the semantic similarity between the original and substitute CUI will be used as a weight to penalize the genetic distance
between two disease codes in the DTW algorithm (see below). The script saves as output two map structures: one (*map_gen_id*)
containing the set of genes associated with each ICD-9 code and another (*map_gen_dist*) containing the corresponding 
penalization weights (as described above). 


**cluster_gen_map.m:**
The mat variables holding the common disease trajectories are loaded (t2, t3,...) in order to be clustered.
The maps formed in the previous step (*map_gen_id*,*map_gen_dist*) are also needed as input 
into the clustering algorith, which is executed by calling the *dtw_GEN_map()* function, described below.
Finally, the clusters are saved in both a text file (*Q_str_thres_GEN.txt*) and matlab variables (*Qmat*, *CIDmat*). 
A file with stats info of the retrieved clusters is also saved separately (*Q_str_thres_GEN_info_srt.txt*) and *str_thres* denotes
the threshold that is implicated in the clustering algorithm to define the clustering granularity.


**dtw_GEN_map().m:**
It aligns two disease trajectories based on the Dynamic Time Warping (DTW) method. The *map_gen_id* and *map_gen_dist*
of the previous script are needed as input in order to form the local distance matrix with the pairwise genetic similarities.
The genetic distance between two codes is calculated by calculating the Jaccard index between the respective sets of genes 
(*map_gen_id*) annotated to each disease, penalized with the corresponding weights (*map_gen_dist*).

**Phenotypic clustering**

**createmap_hpo.m:**
This script creates a map with the annotated sets of phenotypes for all ICD-9 codes involved in the study. 
First, the mat variables t2, t3, etc. of the previous script are loaded (representing the common 
disease trajectories of various lengths) and the list of all ICD-9 codes is extracted. By quering the 
databases on the server, each ICD-9 code is first mapped to the corresponding UMLS concept unique identifier (CUI).
If a CUI is not associated to any phenotype, then a heuristic algorithm is executed, where the most semantically similar
CUI with at least one annotated phenotype is sought. This will substitute the CUI with missing phenotype annotation information
and the semantic similarity between the original and substitute CUI will be used as a weight to penalize the phenotypic distance
between two disease codes in the DTW algorithm (see below). The script saves as output two map structures: one (*map_phen_id*)
containing the set of phenotypes associated with each ICD-9 code and another (*map_phen_dist*) containing the corresponding 
penalization weights (as described above). 


**createmap_hpo_distances.m:**
This script takes as input the mat variables t2, t3, etc with the common disease trajectories and the map named *map_phen_id* 
of the previous script that contains the set of phenotypes associated with each ICD-9 code involved in the study. 
Next, all phenotype terms are put together to form a vector array. By interrogating the semantic_similarity database, 
the script then constructs another map, saved as *map_phen_iic_lch*, that contains the semantic distances between all pairwise
phenotypes of the vector.  


**createmap_dis_hpo_dist.m:**
This script takes as input the mat variables with the common disease trajectories (t2,t3, etc) and the three maps constructed 
in the previous steps (*map_phen_id*, *map_phen_dist*, *map_phen_iic_lch*) and calculates the phenotypic distance 
between all ICD-9 pairs invovled in the disease trajectories of the study. The result is penalized with the corresponding 
weights as calculated by the heuristic algorithm performed in *createmap_hpo.m* for substituting diseases with 
missing phenotypic annotation. A map is finally returned, named *map_dis_phen*, that contains all pairwise distances. 
This script calls for the *BMA_phen()* function described below.


**BMA_phen():**
This script receives as input the sets of phenotypes for the two diseases to be compared (saved in *map_phen_id*) and the 
*map_phen_iic_lch* that contains the semantic distance between two individual phenotypes and by performing the BMA
(Best-Match Average) method, returns the final phenotypic distance between two ICD-9 codes. 

**cluster_phen_map.m:**
The mat variables holding the common disease trajectories are loaded (t2, t3,...) in order to be clustered.
The map *map_dis_phen* formed in the previous steps is also needed as input 
into the clustering algorith, which is executed by calling the *dtw_disPHEN_map()* function, described below.
Finally, the clusters are saved in both a text file (*Q_str_thres_PHEN.txt*) and matlab variables (*Qmat*, *CIDmat*). 
A file with stats info of the retrieved clusters is also saved separately (*Q_str_thres_PHEN_info_srt.txt*) and *str_thres* denotes
the threshold that is implicated in the clustering algorithm to define the clustering granularity.


**dtw_disPHEN_map().m:**
It aligns two disease trajectories based on the Dynamic Time Warping (DTW) method. The map *map_dis_phen* 
of the previous scripts is needed as input in order to form the local distance matrix with the pairwise phenotypic similarities.

**Cluster Evaluation**

**evaluate_clusters_horiz_vert.m**
This script perfoms the horizontal and vertical-wise evaluation of the clusters extracted in the previous steps.
It received as input the following maps constructed as described above, i.e.: *map_sem_sok*, *map_gen_id*, *map_gen_dist*,
*map_dis_phen*,  together with the matlab variables of the specific clusterization (*Qmat*, *CIDmat*).
The horizontal-wise clusterization calls for the *eval_rows_all()* funcion, while the vertical-wise one calls the 
*eval_dis_semdist()* funcion, if the clusters had been retrieved with the semantic (clinical) similarity metric. 
If they had been retrieved with either the genetic or phenotypic, then this function should be replaced with the
*eval_dis_gendist()* or *eval_dis_phendist()*, respectively. Finally, two files are saved with the corresponding vertical 
and horizontal-wise evaluation results, i.e. *eval_str_thres_SEM.txt* and *eval_str_thres_rows_dir.csv*. *str_thres* denotes
the threshold that was implicated in the clustering algorithm to define the clustering granularity.

**eval_rows_all()**
This function takes as input two disease codes and the maps *map_sem_sok*, *map_gen_id*, *map_gen_dist*, *map_dis_phen*
constructed in the previous steps and returns the corresponding clinical, genetic and phenotypic similarity metrics.
It calls three functions: *eval_dis_semdist()*, *eval_dis_gendist()* and *eval_dis_phendist()*, described below.

**eval_dis_semdist()**
This funcion takes as input two disease codes and the *map_sem_sok* and returns the respective semantic distance.

**eval_dis_gendist()**
This funcion takes as input two disease codes and the *map_gen_id*, *map_gen_dist* maps and returns the respective 
genetic distance.

**eval_dis_gendist()**
This funcion takes as input two disease codes and the *map_dis_phen* and returns the respective 
phenotypic distance.
