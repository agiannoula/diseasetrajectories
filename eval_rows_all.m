function [qq1,qq2,qq3] = eval_rows_all(s,t,map_gen_id,map_gen_dist,map_dis_hpo,map_sem_sok,penalflag)

%Disease trajectory clustering software
%Copyright (C) 2020 Alexia Giannoula and Laura I. Furlong, IBI group.
%This file is part of the Disease trajectory and clustering software

%The Disease trajectory and clustering software is free software: you can redistribute it and/or modify it under 
%the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 
%of the License, or (at your option) any later version (https://www.gnu.org/licenses/gpl-3.0.html).
 
%Disease trajectory and clustering software is distributed in the hope that it will be useful,but WITHOUT 
%ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
%See the GNU General Public License for more details.
 
%You should have received a copy of the GNU General Public License
%along with this program.  If not, see <http://www.gnu.org/licenses/>.
 
%How to cite Disease trajectory and clustering software:
%Giannoula, A., Centeno, E., Mayer M.-A., Sanz, F., & Furlong, L. I. A system-level analysis of patient disease 
%trajectories based on clinical, phenotypic and molecular similarities. Submitted to Bioinformatics.

qq1 = eval_dis_gendist(s,t,map_gen_id,map_gen_dist,penalflag);
qq2 = eval_dis_phendist(s,t,map_dis_hpo);
qq3 = eval_dis_semdist(s,t,map_sem_sok);