%Disease trajectory clustering software
%Copyright (C) 2020 Alexia Giannoula and Laura I. Furlong, IBI group.
%This file is part of the Disease trajectory and clustering software

%The Disease trajectory and clustering software is free software: you can redistribute it and/or modify it under 
%the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 
%of the License, or (at your option) any later version (https://www.gnu.org/licenses/gpl-3.0.html).
 
%Disease trajectory and clustering software is distributed in the hope that it will be useful,but WITHOUT 
%ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
%See the GNU General Public License for more details.
 
%You should have received a copy of the GNU General Public License
%along with this program.  If not, see <http://www.gnu.org/licenses/>.
 
%How to cite Disease trajectory and clustering software:
%Giannoula, A., Centeno, E., Mayer M.-A., Sanz, F., & Furlong, L. I. A system-level analysis of patient disease 
%trajectories based on clinical, phenotypic and molecular similarities. Submitted to Bioinformatics.

%%%% map ICD9-CUI, CUI-HPO


clear all

N = 2;
load t2
t = t2;
u2 = unique(table2array(t(:,1:N)));


N = 3;
load t3 
t = t3;
u3 = unique(table2array(t(:,1:N)));

N = 4;
load t4
t = t4;
u4 = unique(table2array(t(:,1:N)));

N = 5;
load t5
t = t5;
u5 = unique(table2array(t(:,1:N)));

N = 6;
load t6
t = t6;
u6 = unique(table2array(t(:,1:N)));

u =[u2; u3; u4; u5; u6];

u = cellstr(u);
u = unique(u);

L = size(u,1)


%setup DATABASES connection with MYSQL
%semantic similarity database
instance = input('Instance for database: ');
username = input('Username for database: ');
password = input('Password for database: ');
conn = database(instance, username, password);

%phenotype database
instancePHEN = input('Instance for phenotype database: ');
usernamePHEN = input('Username for phenotype database: ');
passwordPHEN = input('Password for phenotype database: ');
conn_ph = database(instancePHEN, usernamePHEN, passwordPHEN);


qu1 = "SELECT icd9cm_allsab_diso.cui FROM semantic_similarity.icd9cm_allsab_diso where icd9cm_allsab_diso.code regexp '(^| )";
qu3 = "($| )'";

qu_fin = "SELECT * FROM phenotypes.disease_phenotype WHERE umls_id = '";
qu_fin_c = char(39);

qu_fin_a = "SELECT ytex_similarity_icd9cm_allsab_diso.sokal from semantic_similarity.ytex_similarity_icd9cm_allsab_diso where ytex_similarity_icd9cm_allsab_diso.cui1 = '";
qu_fin_b = "' AND ytex_similarity_icd9cm_allsab_diso.cui2 = '";

qu_allcuis = "SELECT * FROM semantic_similarity.icd9cm_allsab_diso";
res_allcuis = select(conn,qu_allcuis);

keyset = strings(1,L); 
for i=1:L
    str = sprintf('L = %d out of %d',i,L);
    disp(str)
                  
    r = u{i};
    dot_r = strfind(r,'.');
    if (~isempty(dot_r))            
        Lr = length(r);
        r_beg = r(1:dot_r-1);
        r_end = r(dot_r+1:Lr);
        if (dot_r == 2)
            r_beg = strcat('00',r_beg);
        elseif (dot_r == 3)
            r_beg = strcat('0',r_beg);
        elseif (dot_r == 5)   %% included only because Vcodes where substituted with 10. next, delete.
            r_beg = strcat('V',r_beg(3:4));
        end       
        rr = strcat(strcat(r_beg,'\\.'),r_end);
    else
        if (length(r) == 1)
            rr = strcat('00',r);
        elseif (length(r) == 2)
            rr = strcat('0',r);
        else
            rr = r;
        end            
    end        

    query_r=strcat(strcat(qu1,rr),qu3);               
    res_r=select(conn,query_r);

    if (isempty(res_r) ~= 1)

        query_fin = strcat(strcat(qu_fin,res_r.cui),qu_fin_c);        
        res = select(conn_ph,query_fin);   
        
        if isempty(res)                
            disp('empty result from phenotypes query: no phenotypes...');            
            flag(i) = -2;
        else
            phen_id =res.hpo_id;
            phen_descr = res.hpo_name;    
            flag(i) = 1;
            phen_weight(i) = 2;
            phen_num(i) = 0;                        
        end 
        
        %in case of codes/cuis without phenotype
        if (flag(i) == -2)
            disp(flag(i))
                       
            for k=1:size(res_allcuis,1)             
                if (strcmp(res_r.cui,res_allcuis.cui(k)) ~= 1)                    
                   query_list = strcat(strcat(strcat(qu_fin_a,res_r.cui),strcat(qu_fin_b,res_allcuis.cui(k))),qu_fin_c);        
                   res_list = select(conn,query_list);                                
                   dist(k) =res_list.sokal;                                        
                end                 
             end
             t_allcuis = res_allcuis;
             t_allcuis.dist = dist';
             t_allcuis = sortrows(t_allcuis,-4);
             m = 1;
             res_loop = [];
             while isempty(res_loop)
                 qu_loop = strcat(strcat(qu_fin,t_allcuis.cui(m)),qu_fin_c);        
                 res_loop = select(conn_ph,qu_loop);
                 m = m+1;
             end             
             sel = m-1            
             if (sel <= size(t_allcuis,1))
                 flag(i) = 2;
                 phen_id =res_loop.hpo_id;
                 phen_descr = res_loop.hpo_name;    
                 phen_weight(i) = t_allcuis.dist(sel);
                 phen_num(i) = sel;
             else                
                phen_id = '-2';
                phen_descr = 'cui,no-phen even after searching entire database for semantic similarity...';
                phen_weight(i) = -2;
                phen_num(i) = -2;
             end
                 
             clear dist 
        end
                                                                                                                
    elseif (isempty(res_r))                            
               
        query_r=strcat(strcat(qu1,r_beg),qu3);    %check 3dig level          
        res_r2=select(conn,query_r);
                
        if (isempty(res_r2) ~= 1)

            query_fin = strcat(strcat(qu_fin,res_r2.cui),qu_fin_c);        
            res = select(conn_ph,query_fin);   
        
            if isempty(res)                
                disp('empty result from phenotypes query(2): no phenotypes...');            
                flag(i) = -3;                                                                
            else
                phen_id =res.hpo_id;
                phen_descr = res.hpo_name;  
                phen_weight(i) = 3;
                phen_num(i) = -3;
                flag(i) = 3;
            end   
            
            %in case of codes/cuis without phenotype
            if (flag(i) == -3)
                disp(flag(i))
                
                for k=1:size(res_allcuis,1)             
                    if (strcmp(res_r2.cui,res_allcuis.cui(k)) ~= 1)                    
                        query_list = strcat(strcat(strcat(qu_fin_a,res_r2.cui),strcat(qu_fin_b,res_allcuis.cui(k))),qu_fin_c);        
                        res_list = select(conn,query_list);                                
                        dist2(k) =res_list.sokal;                                        
                    end                 
                end
                t_allcuis = res_allcuis;
                t_allcuis.dist = dist2';
                t_allcuis = sortrows(t_allcuis,-4);
                m = 1;
                res_loop = [];
                while isempty(res_loop)
                    qu_loop = strcat(strcat(qu_fin,t_allcuis.cui(m)),qu_fin_c);        
                    res_loop = select(conn_ph,qu_loop);
                    m = m+1;
                end
                sel = m-1
                if (sel <= size(t_allcuis,1))
                    flag(i) = 4;
                    phen_id =res_loop.hpo_id;
                    phen_descr = res_loop.hpo_name;    
                    phen_weight(i) = t_allcuis.dist(sel);
                    phen_num(i) = sel;
                else                
                    phen_id = '-3';
                    phen_descr = 'no cui,no-phen even after searching entire database for semantic similarity...';
                    phen_weight(i) = -3;
                    phen_num(i) = -4;
                end
                clear dist 
            end
                        
        else                 
            disp(r_beg)
            input('STILL empty result from semantic query: no CUI for 5 dig nor 3 dig...')     
            phen_id = -1;
            phen_descr = 'no-cui,no-phen';
            flag(i) = -1;                
            phen_weight(i) = -1;
            phen_num(i) = -1;
        end
        
    else
        input('problem...')
    end    

    keyset(i) = r;       
    valueset_id{i} = phen_id;
    valueset_descr{i} = phen_descr;            
    
    
end

phen_flag = flag;

map_phen_id = containers.Map(cellstr(keyset),valueset_id);
map_phen_descr = containers.Map(cellstr(keyset),valueset_descr);
map_phen_dist = containers.Map(cellstr(keyset),phen_weight);
map_phen_order = containers.Map(cellstr(keyset),phen_num);

save map_phen_id map_phen_id 
save map_phen_dist map_phen_dist


close(conn);
close(conn_ph);

