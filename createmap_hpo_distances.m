%Disease trajectory clustering software
%Copyright (C) 2020 Alexia Giannoula and Laura I. Furlong, IBI group.
%This file is part of the Disease trajectory and clustering software

%The Disease trajectory and clustering software is free software: you can redistribute it and/or modify it under 
%the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 
%of the License, or (at your option) any later version (https://www.gnu.org/licenses/gpl-3.0.html).
 
%Disease trajectory and clustering software is distributed in the hope that it will be useful,but WITHOUT 
%ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
%See the GNU General Public License for more details.
 
%You should have received a copy of the GNU General Public License
%along with this program.  If not, see <http://www.gnu.org/licenses/>.
 
%How to cite Disease trajectory and clustering software:
%Giannoula, A., Centeno, E., Mayer M.-A., Sanz, F., & Furlong, L. I. A system-level analysis of patient disease 
%trajectories based on clinical, phenotypic and molecular similarities. Submitted to Bioinformatics.

%%%% map ICD9-CUI, CUI-HPO


clear all

N = 2;
load t2
t = t2;
u2 = unique(table2array(t(:,1:N)));


N = 3;
load t3 
t = t3;
u3 = unique(table2array(t(:,1:N)));

N = 4;
load t4
t = t4;
u4 = unique(table2array(t(:,1:N)));

N = 5;
load t5
t = t5;
u5 = unique(table2array(t(:,1:N)));

N = 6;
load t6
t = t6;
u6 = unique(table2array(t(:,1:N)));

u =[u2; u3; u4; u5; u6];

u = cellstr(u);
u = unique(u);

N = size(u,1)


load map_phen_id

ph_list = [];
for i=1:N
    r = u{i};
    ph_set{i}=map_phen_id(r);    
    s(i) = size(ph_set{i},1);
    ph_list = [ph_list; ph_set{i}];    
end

ph_list_u = unique(ph_list);

%setup DATABASE connection with MYSQL
%semantic similarity database
instance = input('Instance for database: ');
username = input('Username for database: ');
password = input('Password for database: ');

conn = database(instance, username, password);


qu_init = "SELECT hpo_hposab_all.cui  FROM semantic_similarity.hpo_hposab_all where hpo_hposab_all.code LIKE '%";
qu_finLIKE = strcat('%',char(39));
qu_fin = char(39);

qu_fin_a = "SELECT ytex_similarity_hpo_hposab_all.iic_lch from semantic_similarity.ytex_similarity_hpo_hposab_all where ytex_similarity_hpo_hposab_all.cui1 = '";
qu_fin_b = "' AND ytex_similarity_hpo_hposab_all.cui2 = '";


keyset = strings(L,L); valueset = ones(L,L);  
flag = zeros(L,L); q = zeros(L,L);

for i=1:L  
    for j=1:L
        
        r0 = ph_list_u{i};
        r = strcat('HP:',r0);
        t0 = ph_list_u{j};  
        t = strcat('HP:',t0);        
        
        if (i~=j)
            
            if (flag(i,j) == 0)                                        
                query_r=strcat(strcat(qu_init,r),qu_finLIKE);                           
                res_r=select(conn,query_r);
                query_t=strcat(strcat(qu_init,t),qu_finLIKE);                           
                res_t=select(conn,query_t);

                sr = size(res_r,1); st = size(res_t,1);

                if (sr == 1 & st == 1)
                   q(i,j) = 1;
                   query_fin = strcat(strcat(strcat(qu_fin_a,res_r.cui),strcat(qu_fin_b,res_t.cui)),qu_fin);        
                   res = select(conn,query_fin);            
                   dist =res.iic_lch;
                elseif (sr > 1 & st == 1)
                   q(i,j) = 2;
                   mdist = [];
                   for k = 1:sr
                        query_fin = strcat(strcat(strcat(qu_fin_a,res_r.cui(k)),strcat(qu_fin_b,res_t.cui)),qu_fin);        
                        res = select(conn,query_fin);            
                        mdist = [mdist res.iic_lch];
                   end
                   dist = max(mdist);
                elseif (sr == 1 & st > 1)
                   q(i,j) = 3;
                   mdist = [];
                   for k = 1:st
                        query_fin = strcat(strcat(strcat(qu_fin_a,res_r.cui),strcat(qu_fin_b,res_t.cui(k))),qu_fin);        
                        res = select(conn,query_fin);            
                        mdist = [mdist res.iic_lch];
                   end
                   dist = max(mdist);
                elseif (sr > 1 & st > 1)
                   q(i,j) = 4;
                   mdist = [];
                   for k = 1:sr
                       for m = 1:st
                            query_fin = strcat(strcat(strcat(qu_fin_a,res_r.cui(k)),strcat(qu_fin_b,res_t.cui(m))),qu_fin);        
                            res = select(conn,query_fin);            
                            mdist = [mdist res.iic_lch];
                       end
                   end
                   dist = max(mdist);                                           
                elseif (sr < 1 | st < 1)    
                   q(i,j) = 5;
                   dist = -1;                    
                end    
                valueset(i,j) = dist;                
                valueset(j,i) = dist;  
                strkey1 = strcat(strcat(r,'_'),t);
                strkey2 = strcat(strcat(t,'_'),r); 
                keyset(i,j) = strkey1;               
                flag(i,j) = 1;
                keyset(j,i) = strkey2;        
                flag(j,i) = 1;   
            end
        else                           
            strkey1 = strcat(strcat(r,'_'),t);        
            keyset(i,j) = strkey1;               
            flag(i,j) = 1;
        end
        
        clear dist mdist      
       
    end
    
end
    
  
map_phen_iic_lch = containers.Map(cellstr(keyset),valueset);

save map_phen_iic_lch map_phen_iic_lch

close(conn);




