#Disease trajectory clustering software
#Copyright (C) 2020 Alexia Giannoula and Laura I. Furlong, IBI group.
#This file is part of the Disease trajectory and clustering software

#The Disease trajectory and clustering software is free software: you can redistribute it and/or modify it under 
#the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 
#of the License, or (at your option) any later version (https://www.gnu.org/licenses/gpl-3.0.html).
 
#Disease trajectory and clustering software is distributed in the hope that it will be useful,but WITHOUT 
#ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
#See the GNU General Public License for more details.
 
#You should have received a copy of the GNU General Public License
#along with this program.  If not, see <http://www.gnu.org/licenses/>.
 
#How to cite Disease trajectory and clustering software:
#Giannoula, A., Centeno, E., Mayer M.-A., Sanz, F., & Furlong, L. I. A system-level analysis of patient disease 
#trajectories based on clinical, phenotypic and molecular similarities. Submitted to Bioinformatics.


import os, sys, re
import scipy.stats

# READ DISEASE PAIRS
pairs_file = sys.argv[1]
fd = open(pairs_file)
disease_pairs = set()

for line in fd:

        disease1, disease2, npat, c12, age1, age2 = line.strip().split("\t")

	disease_pairs.add( (disease1, disease2, int(npat), float(c12), float(age1), float(age2)) )
fd.close()

# READ PATIENTS FILE
patients_file = sys.argv[2]

fd = open(patients_file, "r")

all_patients = []

for line in fd:
	diseases = set([ x.strip() for x in line.strip().split(" ") ])	
	all_patients.append(diseases)
fd.close()


for dis1, dis2, npat, c12, age1, age2 in disease_pairs:

	num_d1 = 0
	num_d2 = 0
	num_d1d2 = 0
	num_not = 0

	for patient in all_patients:
	
		if dis1 not in patient and dis2 not in patient:
			num_not += 1
		elif dis1 not in patient and dis2 in patient:
			num_d2 += 1
		elif dis1 in patient and dis2 not in patient:
			num_d1 += 1
		elif dis1 in patient and dis2 in patient:
			num_d1d2 += 1
			
	cij = num_d1d2
	N = len(all_patients)
	Pi = num_d1
	Pj = num_d2
	
	fisher = scipy.stats.fisher_exact([ [num_d1d2, num_d1], [num_d2, num_not] ])

	
	print dis1, dis2, npat, c12, age1, age2, fisher[1], num_d1d2, num_d1, num_d2, num_not





	

